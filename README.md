# The Stacker
Adds a new character, the Stacker. They have a fully playable more-or-less-guideline matrix.
Clearing lines deals damage split among all enemies. Unblocked attack damage is added as garbage lines.
Topping out causes you to lose HP equal to your stack height, and then resets the matrix.

Power scaling is primarily accomplished through generating more piece cards, which are autoplay cards that add a piece
to your queue and draw another card when drawn. Strength also increases combo damage. A family of power cards known as
Protocol cards add effects such as healing, block gain, extra damage, etc. to special line clears (t spins, PCs, quads,
combo x, b2b x) and can be upgraded multiple times, but only one can be in effect at a time.

Gravity cards are another class of cards which interact with the current gravity. Various cards offer ways to gain
gravity, and some cards require a minimum gravity to play or scale based on the gravity. Gravity also increases piece
fall speed, though you'd need a *lot* to reach 20G.

Opener cards are a class of attack cards which can only be played as the first card each combat. They build an opener
and queue pieces necessary to complete it. After that, they become unplayable and are only good as fodder for other
(unimplemented) cards which utilize their side effects.

The mod is currently far from finished. It only has few dozen cards finished and also needs lots of art to be created.

Forked from https://github.com/Gremious/StS-DefaultModBase
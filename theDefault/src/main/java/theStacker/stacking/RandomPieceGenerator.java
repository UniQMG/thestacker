package theStacker.stacking;

public class RandomPieceGenerator implements PieceGenerator {
    @Override
    public Piece.PieceType next() {
        return Piece.PieceType.random();
    }
}

package theStacker.stacking;

import static theStacker.stacking.Mino.OUT_OF_BOUNDS;

public class Board {
    protected Mino[][] grid;

    public Board(int rows, int cols) {
        this.grid = new Mino[rows][cols];
        for (int y = 0; y < rows; y++)
            for (int x = 0; x < cols; x++)
                this.grid[y][x] = new Mino();
    }

    public int getRows() {
        return grid.length;
    }

    public int getCols() {
        return grid[0].length;
    }

    public Mino getMino(int row, int col) {
        if (row < 0 || col < 0 || row >= this.grid.length || col >= this.grid[0].length)
            return OUT_OF_BOUNDS;
        return this.grid[row][col];
    }

    public boolean canPut(int row, int col, Board board) {
        for (int y = row; y < row + board.getRows(); y++) {
            for (int x = col; x < col + board.getCols(); x++) {
                if (!board.grid[y - row][x - col].isFilled()) continue;
                if (y < 0 || y >= getRows()) return false;
                if (x < 0 || x >= getCols()) return false;
                if (this.grid[y][x].isFilled()) return false;
            }
        }
        return true;
    }

    /**
     * Places one board into another. The given coordinates are the
     * top left origin of the placement.
     * @param row the row to start placing in
     * @param col the column to start placing in
     * @param board the board to place
     * @return The lowest y value that was filled
     */
    public int put(int row, int col, Board board) {
        int minY = Integer.MAX_VALUE;
        for (int y = row; y < row + board.getRows(); y++) {
            for (int x = col; x < col + board.getCols(); x++) {
                if (x < 0 || y < 0 || x >= getCols() || y >= getRows()) continue;
                if (!board.grid[y - row][x - col].isFilled()) continue;
                this.grid[y][x].set(this, board.grid[y-row][x-col]);
                minY = Math.min(minY, y);
            }
        }
        return minY;
    }

    /**
     * Gets the highest filled block on the board
     * @return the y value of the highest point on the board
     */
    public int getStackHeight() {
        for (int y = this.getRows()-1; y >= 0; y--)
            for (int x = 0; x < this.getCols(); x++)
                if (this.grid[y][x].isFilled())
                    return y;
        return 0;
    }

    /**
     * Creates a copy of this board
     * @return the copied board
     */
    public Board copy() {
        Board copy = new Board(this.getRows(), this.getCols());
        for (int y = 0; y < this.getRows(); y++)
            for (int x = 0; x < this.getCols(); x++)
                copy.grid[y][x].set(this, this.grid[y][x]);
        return copy;
    }
}

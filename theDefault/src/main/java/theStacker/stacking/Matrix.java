package theStacker.stacking;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.evacipated.cardcrawl.modthespire.lib.SpireConfig;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.animations.TalkAction;
import com.megacrit.cardcrawl.actions.common.*;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.powers.StrengthPower;
import theStacker.TheStackerMod;
import theStacker.cards.status.PieceCard;
import theStacker.powers.GravityPower;
import theStacker.powers.stacking.ComboPower;
import theStacker.powers.stacking.IncomingGarbagePower;
import theStacker.powers.stacking.MatrixDamageAbsorbPower;
import theStacker.relics.thestacker.Scaffolding;
import theStacker.util.Formulas;
import theStacker.util.MicroEvent;

import java.util.*;
import java.util.function.Consumer;

import static theStacker.TheStackerMod.logger;
import static theStacker.stacking.Controls.GameKey.*;
import static theStacker.stacking.Piece.PieceType.T;

public class Matrix {
    public final static int BASE_GRAVITY = 1;

    private final MatrixRenderer renderer = new MatrixRenderer();
    private final Controls controls = new Controls(this);
    public final Map<Controls.GameKey, Integer> keymap;
    public PieceGenerator generator = new SevenBagGenerator();
    public Queue<Piece> queue = new ArrayDeque<>();
    public Board board = new Board(40, 10);
    public Piece piece = null;
    public Piece hold;
    public final AbstractCreature owner;
    public int gravity = BASE_GRAVITY; // rows per 60 frames
    public int piecesPlaced = 0; // the total number of pieces placed, even after a reset.
    public boolean enableHold = true;
    public boolean held = false;

    /**
     * A global listener that is called when a new matrix is created, generally for
     * attaching permanent handlers to it.
     */
    public static final MicroEvent<Matrix> create = new MicroEvent<>();
    /**
     * Listener called after locking a piece
     */
    public final MicroEvent<LineClearInfo> pieceLock = new MicroEvent<>();
    /**
     * Listeners for various key events. Cancellable by calling the provided callback.
     */
    public final EnumMap<Controls.GameKey, MicroEvent<Consumer<Controls.ControlEvent>>> input;
    /**
     * Listener called on every tick of the matrix. Note that this is called every *graphical*
     * tick, not every update tick (update ticks are always 60hz)
     */
    public final MicroEvent<Void> nextTick = new MicroEvent<>();

    public Matrix(AbstractCreature owner) {
        keymap = new EnumMap<>(Controls.GameKey.class);
        keymap.put(MOVE_LEFT, Input.Keys.A);
        keymap.put(MOVE_RIGHT, Input.Keys.D);
        keymap.put(SOFT_DROP, Input.Keys.S);
        keymap.put(HARD_DROP, Input.Keys.W);
        keymap.put(ROTATE_CCW, Input.Keys.LEFT);
        keymap.put(ROTATE_CW, Input.Keys.RIGHT);
        keymap.put(ROTATE_180, Input.Keys.UP);
        keymap.put(HOLD, Input.Keys.DOWN);

        input = new EnumMap<>(Controls.GameKey.class);
        for (Controls.GameKey value : values())
            input.put(value, new MicroEvent<>());

        this.owner = owner;
        SpireConfig config = TheStackerMod.getConfig();
        this.controls.DAS = config.getInt("DAS");
        this.controls.ARR = config.getInt("ARR");
        logger.info(String.format("DAS %s ARR %s", this.controls.DAS, this.controls.ARR));

        nextTick.listen(new Object(), () -> {
            if (AbstractDungeon.getMonsters() == null) return true;

            if (!this.owner.hasPower(MatrixDamageAbsorbPower.POWER_ID)) {
                AbstractDungeon.actionManager.addToBottom(
                    new ApplyPowerAction(owner, owner, new MatrixDamageAbsorbPower(owner, 0))
                );
            }

            if (!this.owner.hasPower(ComboPower.POWER_ID)) {
                AbstractDungeon.actionManager.addToBottom(
                    new ApplyPowerAction(owner, owner, new ComboPower(owner, 0))
                );
            }

            if (!this.owner.hasPower(IncomingGarbagePower.POWER_ID)) {
                AbstractDungeon.actionManager.addToBottom(
                    new ApplyPowerAction(owner, owner, new IncomingGarbagePower(owner, 0))
                );
            }

            return false;
        });

        create.fire(this);
    }

    boolean playedPiecesThisTurn = false;
    public void onTurnEnd() {
        if (playedPiecesThisTurn) {
            playedPiecesThisTurn = false;
            return;
        }
        this.combo = 0;
        updateCombo();
    }

    private AbstractCard lastHoverCard;
    private Piece pieceCardHeld;

    // y is up, origin bottom left
    private long time = System.currentTimeMillis();
    public void render(SpriteBatch spriteBatch) {
        this.nextTick.fire(null);

        if (owner instanceof AbstractPlayer) {
            AbstractCard card = ((AbstractPlayer) owner).hand.getHoveredCard();
            if (card != lastHoverCard) {
                lastHoverCard = card;
                pieceCardHeld = card instanceof PieceCard
                    ? Piece.get(((PieceCard) card).getPiece())
                    : null;
            }
        } else {
            pieceCardHeld = null;
        }

        if (owner.hasPower(GravityPower.POWER_ID)) {
            boolean scaffold = owner instanceof AbstractPlayer && ((AbstractPlayer) owner).hasRelic(Scaffolding.ID);
            AbstractPower gravity = owner.getPower(GravityPower.POWER_ID);
            this.gravity = (int) (gravity.amount * (scaffold ? 1 : 0.25f)) + BASE_GRAVITY;
        } else {
            this.gravity = BASE_GRAVITY;
        }

        long timeNow = System.currentTimeMillis();
        // Time may be spent not rendering the matrix, skip any big gaps.
        if (timeNow - time > 150)
            time = timeNow;

        while (timeNow > time) {
            time += 17;
            controls.frame();
        }

        renderer.render(spriteBatch, this);
    }

    public Piece getPieceForHeldCard() {
        return pieceCardHeld;
    }

    private void gameover() {
        this.gameover(false);
    }
    private void gameover(boolean reusePiece) {
        this.gameover(this.board.getStackHeight(), reusePiece);
    }
    private void gameover(int damage, boolean reusePiece) {
        logger.info("Game over! Damage: " + damage);
        this.reset(reusePiece);

        AbstractDungeon.actionManager.addToBottom(new DamageAction(
            owner,
            new DamageInfo(
                owner,
                damage,
                DamageInfo.DamageType.HP_LOSS
            ),
            AbstractGameAction.AttackEffect.BLUNT_HEAVY
        ));
    }
    public void reset(boolean reusePiece) {
        for (int y = 0; y < this.board.getRows(); y++)
            for (int x = 0; x < this.board.getCols(); x++)
                this.board.grid[y][x].reset();
        if (!reusePiece)
            nextPiece();
        this.combo = 0;
        this.updateCombo();
    }

    public int getAttackPower(int lines) {
        AbstractPower power = owner.getPower(StrengthPower.POWER_ID);
        int strength = power == null ? 0 : power.amount;
        return Formulas.lineClearDamage(lines, combo, strength);
    }

    private int combo = 0;
    private int b2b = 0;
    public void lockPiece() {
        if (this.piece == null) throw new NullPointerException("tfw you misdrop null");
        final Piece lockedPiece = piece;

        int maxY = board.put(piece.y, piece.x, piece);
        this.piece = null;
        if (maxY > 20) {
            gameover(); // Lock out!
            return;
        }

        playedPiecesThisTurn = true;

        boolean tSpin = false;
        boolean mini = false;

        if (lockedPiece.type == T) {
//            boolean immobile =
//                !lockedPiece.canMove(board, -1, 0) &&
//                !lockedPiece.canMove(board, 1, 0) &&
//                !lockedPiece.canMove(board, 0, 1);

            int corners = 0;
            for (int[] ints : new int[][]{{0, 0}, {2, 0}, {2, 2}, {0, 2}}) {
                int board_x = lockedPiece.x + ints[0];
                int board_y = lockedPiece.y + ints[1];
                corners += this.board.getMino(board_y, board_x).isFilled() ? 1 : 0;
            }

            mini = lockedPiece.lastMovement == Piece.MovementType.KICK_ROTATE;
            tSpin = corners >= 3 || mini;
        }

        this.piecesPlaced++;
        ArrayList<Integer> cleared = new ArrayList<>();
        checkRows: for (int y = 0; y < board.getRows(); y++) {
            for (int x = 0; x < board.getCols(); x++)
                if (!board.getMino(y, x).isFilled() || board.getMino(y, x).unclearable)
                    continue checkRows;
            cleared.add(y);
        }
        Collections.reverse(cleared);
        for (int y_shift: cleared)
            for (int y = y_shift; y < board.getRows()-1; y++)
                for (int x = 0; x < board.getCols(); x++)
                    board.grid[y][x].set(board, this.board.grid[y+1][x]);

        boolean perfectClear = true;
        checkPC: for (int y = 0; y < board.getRows(); y++) {
            for (int x = 0; x < board.getCols(); x++) {
                if (board.getMino(y,x).isFilled() && !board.getMino(y,x).unclearable) {
                    perfectClear = false;
                    break checkPC;
                }
            }
        }

        if (cleared.size() > 0 && (tSpin || cleared.size() >= 4)) {
            b2b++;
        } else {
            b2b = 0;
        }

        if (cleared.size() == 0) {
            combo = 0;
            this.addGarbageFromPower();
        } else {
            combo++;
            int monsters = Math.max(AbstractDungeon.getMonsters().monsters.size(), 1);
            float damage = getAttackPower(cleared.size()) / (float) monsters;

            AbstractDungeon.actionManager.addToBottom(new DamageAllEnemiesAction(
                owner,
                DamageInfo.createDamageMatrix((int) Math.ceil(damage),true, false),
                DamageInfo.DamageType.NORMAL,
                AbstractGameAction.AttackEffect.SMASH
            ));
        }

        boolean triple = false;
        if (tSpin && cleared.size() >= 3) {
            triple = true;
            mini = false;
        }

        updateCombo();
        nextPiece();
        pieceLock.fire(new LineClearInfo(this, lockedPiece, cleared.size(), combo, b2b, tSpin, mini, perfectClear));
        logger.info(String.format("Lines: %s (%s combo, %s b2b) [tspin: %s, mini: %s, PC: %s]", cleared.size(), combo, b2b, tSpin, mini, perfectClear));


        StringBuilder debug = new StringBuilder();
        if (combo > 0) debug.append("Combo ").append(combo).append(" NL ");
        if (b2b > 0) debug.append("B2B ").append(b2b).append(" NL ");
        if (tSpin) debug.append("T-Spin");
        if (mini) debug.append(" mini");
        if (triple) debug.append(" triple");
        if (tSpin) debug.append(" NL ");
        if (perfectClear) debug.append("Perfect clear");

        if (debug.length() > 0)
        AbstractDungeon.actionManager.addToBottom(new TalkAction(
            true,
            debug.toString(),
            1.0F,
            2.0F
        ));
    }

    private void updateCombo() {
        AbstractPower power = owner.getPower(ComboPower.POWER_ID);
        int level = power == null ? 0 : power.amount;
        AbstractDungeon.actionManager.addToBottom(
            new ApplyPowerAction(owner, owner, new ComboPower(owner, combo - level))
        );
    }

    void nextPiece() {
        piece = null;
        if (queue.size() > 0) {
            piece = queue.poll();
            piece.reposition();
            held = false;
            if (!piece.canMove(this.board, 0, 0)) {
                this.gameover(true); // Block out!
            }
        }
    }
    public void queuePiece(Piece piece) {
        this.queue.add(piece);
        if (this.piece == null)
            this.nextPiece();
    }
    public Piece.PieceType getNextPieceFromGenerator() {
        return this.generator.next();
    }

    public int getIncomingGarbage() {
        AbstractPower garbage = owner.getPower(IncomingGarbagePower.POWER_ID);
        if (garbage == null) return 0;
        return garbage.amount;
    }
    public void addGarbageFromPower() {
        if (combo >= 1) return;
        AbstractPower garbage = owner.getPower(IncomingGarbagePower.POWER_ID);
        if (garbage != null) {
            addGarbage(garbage.amount, 1, false);
            garbage.reducePower(garbage.amount);
        }
    }
    public void addGarbage(int amount, int holes, boolean dark) {
        if (holes < 0 || holes >= this.board.getCols())
            throw new IllegalArgumentException("Invalid hole count: " + holes);

        logger.info("Add garbage " + amount);
        int totalHeight = amount + board.getStackHeight();
        if (totalHeight > board.getRows()) {
            gameover(totalHeight, true); // Top out!
            return;
        }

        // Shift up lines
        for (int y = this.board.getRows()-1; y >= amount; y--)
            for (int x = 0; x < board.getCols(); x++)
                board.grid[y][x].set(null, board.grid[y - amount][x]);

        // Add garbage
        ArrayList<Integer> unfilled = generateGaps(holes);
        for (int y = 0; y < amount; y++) {
            if (Math.random() > 0.5)
                unfilled = generateGaps(holes);
            for (int x = 0; x < board.getCols(); x++)
                board.grid[y][x].set(null, unfilled.contains(x) ? Mino.EMPTY : (dark ? Mino.DARK_GARBAGE : Mino.GARBAGE));
        }
    }
    private ArrayList<Integer> generateGaps(int holes) {
        ArrayList<Integer> filled = new ArrayList<>(this.board.getCols());
        for (int i = 0; i < this.board.getCols(); i++)
            filled.add(i);

        ArrayList<Integer> unfilled = new ArrayList<>(holes);
        for (int i = 0; i < holes; i++)
            unfilled.add(filled.remove((int) (Math.random() * filled.size())));

        return unfilled;
    }
}

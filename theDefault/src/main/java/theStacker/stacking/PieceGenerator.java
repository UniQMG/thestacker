package theStacker.stacking;

public interface PieceGenerator {
    Piece.PieceType next();
}

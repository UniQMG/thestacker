package theStacker.stacking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SevenBagGenerator implements PieceGenerator {
    private ArrayList<Piece.PieceType> bag = new ArrayList<>();

    @Override
    public Piece.PieceType next() {
        if (bag.size() == 0) {
            bag.addAll(Arrays.asList(Piece.PieceType.values()));
            Collections.shuffle(bag);
        }
        return bag.remove(bag.size()-1);
    }
}

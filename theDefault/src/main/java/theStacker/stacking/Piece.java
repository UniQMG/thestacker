package theStacker.stacking;

import com.badlogic.gdx.graphics.Color;

import java.util.function.BiConsumer;

import static theStacker.stacking.Piece.MovementType.*;
import static theStacker.stacking.Piece.PieceType.*;

public class Piece extends Board {
    private final static Kick[] jlstzKicks = new Kick[]{
        new Kick(0, 1, new int[][]{{0, 0}, {-1, 0}, {-1, 1}, { 0,-2}, {-1,-2}}),
        new Kick(1, 0, new int[][]{{0, 0}, { 1, 0}, { 1,-1}, { 0, 2}, { 1, 2}}),
        new Kick(1, 2, new int[][]{{0, 0}, { 1, 0}, { 1,-1}, { 0, 2}, { 1, 2}}),
        new Kick(2, 1, new int[][]{{0, 0}, {-1, 0}, {-1, 1}, { 0,-2}, {-1,-2}}),
        new Kick(2, 3, new int[][]{{0, 0}, { 1, 0}, { 1, 1}, { 0,-2}, { 1,-2}}),
        new Kick(3, 2, new int[][]{{0, 0}, {-1, 0}, {-1,-1}, { 0, 2}, {-1, 2}}),
        new Kick(3, 0, new int[][]{{0, 0}, {-1, 0}, {-1,-1}, { 0, 2}, {-1, 2}}),
        new Kick(0, 3, new int[][]{{0, 0}, { 1, 0}, { 1, 1}, { 0,-2}, { 1,-2}})
    };
    private final static Kick[] iKicks = new Kick[]{
        new Kick(0, 1, new int[][]{{0, 0}, {-2, 0}, { 1, 0}, {-2,-1}, { 1, 2}}),
        new Kick(1, 0, new int[][]{{0, 0}, { 2, 0}, {-1, 0}, { 2, 1}, {-1,-2}}),
        new Kick(1, 2, new int[][]{{0, 0}, {-1, 0}, { 2, 0}, {-1, 2}, { 2,-1}}),
        new Kick(2, 1, new int[][]{{0, 0}, { 1, 0}, {-2, 0}, { 1,-2}, {-2, 1}}),
        new Kick(2, 3, new int[][]{{0, 0}, { 2, 0}, {-1, 0}, { 2, 1}, {-1,-2}}),
        new Kick(3, 2, new int[][]{{0, 0}, {-2, 0}, { 1, 0}, {-2,-1}, { 1, 2}}),
        new Kick(3, 0, new int[][]{{0, 0}, { 1, 0}, {-2, 0}, { 1,-2}, {-2, 1}}),
        new Kick(0, 3, new int[][]{{0, 0}, {-1, 0}, { 2, 0}, {-1, 2}, { 2,-1}})
    };
    private final static Kick[] oKicks = new Kick[]{
        new Kick(0, 1, new int[][]{{0, 0}}),
        new Kick(1, 0, new int[][]{{0, 0}}),
        new Kick(1, 2, new int[][]{{0, 0}}),
        new Kick(2, 1, new int[][]{{0, 0}}),
        new Kick(2, 3, new int[][]{{0, 0}}),
        new Kick(3, 2, new int[][]{{0, 0}}),
        new Kick(3, 0, new int[][]{{0, 0}}),
        new Kick(0, 3, new int[][]{{0, 0}})
    };

    public enum PieceType {
        I, J, L, O, Z, T, S;
        public static PieceType random() {
            PieceType[] values = PieceType.values();
            int index = (int) (Math.random() * values.length);
            return values[index];
        }
    }
    public enum LimitedRotateDirection { CW, CCW }
    public enum RotateDirection {
        CW(1), CCW(-1), R180(2);

        public final int steps;
        RotateDirection(int steps) {
            this.steps = steps;
        }
    }
    public static Piece get(PieceType pieceType) {
        switch (pieceType) {
            case I: return new Piece(I,4, Mino.I.color, new int[][]{{0,2},{1,2},{2,2},{3,2}}, iKicks, null);
            case J: return new Piece(J,3, Mino.J.color, new int[][]{{0,1},{0,2},{1,1},{2,1}}, jlstzKicks, null);
            case L: return new Piece(L,3, Mino.L.color, new int[][]{{2,2},{0,1},{1,1},{2,1}}, jlstzKicks, null);
            case O: return new Piece(O,4, Mino.O.color, new int[][]{{1,1},{1,2},{2,1},{2,2}}, oKicks, null);
            case Z: return new Piece(Z,3, Mino.Z.color, new int[][]{{1,1},{2,1},{0,2},{1,2}}, jlstzKicks, null);
            case T: return new Piece(T,3, Mino.T.color, new int[][]{{1,2},{0,1},{1,1},{2,1}}, jlstzKicks, null);
            case S: return new Piece(S,3, Mino.S.color, new int[][]{{0,1},{1,1},{1,2},{2,2}}, jlstzKicks, null);
        }
        throw new RuntimeException("Unknown piece: " + pieceType);
    }

    public final PieceType type; // nullable if not a preset piece
    private int rotation = 0;
    public int x = 0;
    public int y = 0;

    public enum MovementType { SHIFT, FALL, ROTATE, KICK_ROTATE }
    private Color color;
    private int[][] minos;
    public final Kick[] kicks;
    private BiConsumer<Board, Mino> onClear;
    MovementType lastMovement = MovementType.FALL;
    int fallProgress = 0; // gravity is in blocks per 60 frames
    int lockProgress = 0; // in frames
    int lockResets = 0;

    private Piece(Piece piece) {
        super(piece.getRows(), piece.getCols());
        this.type = piece.type;
        this.x = piece.x;
        this.y = piece.y;
        this.color = piece.color;
        this.minos = piece.minos;
        this.kicks = piece.kicks;
        this.rotation = piece.rotation;
        this.grid = piece.grid;
        this.fallProgress = piece.fallProgress;
        this.lockProgress = piece.lockProgress;
        this.lockResets = piece.lockResets;
        this.onClear = piece.onClear;
        this.lastMovement = piece.lastMovement;
    }
    public Piece(PieceType type, int size, Color color, int[][] minos, Kick[] kicks, BiConsumer<Board, Mino> onClear) {
        super(size, size);
        this.type = type;
        this.color = color;
        this.minos = minos;
        this.kicks = kicks;
        this.onClear = onClear;
        this.reposition();
    }

    public Piece getGhost(Board board) {
        Piece ghost = new Piece(this); // todo: reduce allocation
        while (ghost.canMove(board, 0, -1))
            ghost.move(0, -1);
        return ghost;
    }

    /**
     * Moves the piece back to its initial spawning position (e.g. if unheld)
     * and resets rotation
     */
    public void reposition() {
        for (int y = 0; y < this.getRows(); y++)
            for (int x = 0; x < this.getCols(); x++)
                this.getMino(y, x).reset();
        for (int[] mino: minos) {
            int x = mino[0];
            int y = mino[1];
            this.getMino(y, x).set(this, new Mino(true, color));
            this.getMino(y, x).onClear = onClear;
        }
        this.x = 5 - this.getCols()/2;
        this.y = 21;
        this.rotation = 0;
        this.fallProgress = 0;
        this.lockProgress = 0;
        this.lastMovement = FALL;
    }

    private int getRotationNumber(RotateDirection dir) {
        int rotationNumber = this.rotation + dir.steps;
        while (rotationNumber > 3) rotationNumber -= 4;
        while (rotationNumber < 0) rotationNumber += 4;
        return rotationNumber;
    }

    private Board getRotated(LimitedRotateDirection dir) {
        Board copy = this.copy();
        for (int y = 0; y < this.getCols(); y++) {
            for (int x = 0; x < this.getRows(); x++) {
                float pivot_x = this.getCols()/2f;
                float pivot_y = this.getRows()/2f;

                int c_x = (int) (dir == LimitedRotateDirection.CCW
                    ? (y - pivot_y) + pivot_y
                    : -(y - pivot_y) + pivot_y - 1);

                int c_y = (int) (dir == LimitedRotateDirection.CCW
                    ? -(x - pivot_x) + pivot_x - 1
                    : (x - pivot_x) + pivot_x);

                copy.grid[y][x] = this.grid[c_y][c_x];
            }
        }
        return copy;
    }

    private class RotateResult {
        boolean success = false;
        boolean usedKick = false;
    }

    private RotateResult tryRotate(Board board, RotateDirection dir, boolean testOnly) {
        Mino[][] original = this.grid;
        switch (dir) {
            case CCW:
                this.grid = this.getRotated(LimitedRotateDirection.CCW).grid;
                break;

            case CW:
                this.grid = this.getRotated(LimitedRotateDirection.CW).grid;
                break;

            case R180:
                this.grid = this.getRotated(LimitedRotateDirection.CW).grid;
                this.grid = this.getRotated(LimitedRotateDirection.CW).grid;
                break;
        }

        RotateResult result = new RotateResult();
        int kick_x = 0, kick_y = 0;
        if (board.canPut(this.y, this.x, this)) {
            result.success = true;
        } else {
            kicktest: for (Kick kick : kicks) {
                if (kick.from != this.rotation || kick.to != this.getRotationNumber(dir))
                    continue;

                for (int[] test : kick.tests) {
                    kick_x = test[0];
                    kick_y = test[1];
                    if (board.canPut(this.y + kick_y, this.x + kick_x, this)) {
                        result.success = true;
                        result.usedKick = true;
                        break kicktest;
                    }

                }
            }
        }

        if (testOnly) {
            this.grid = original;
        } else {
            this.rotation = this.getRotationNumber(dir);
            this.x += kick_x;
            this.y += kick_y;
        }
        return result;
    }
    public boolean canRotate(Board board, RotateDirection dir) {
        return this.tryRotate(board, dir, true).success;
    }
    public void rotate(Board board, RotateDirection dir) {
        RotateResult result = this.tryRotate(board, dir, false);
        if (!result.success) return;
        lastMovement = result.usedKick ? KICK_ROTATE : ROTATE;
    }

    public boolean canMove(Board board, int dx, int dy) {
        return board.canPut(y+dy, x+dx, this);
    }
    public void move(int dx, int dy) {
        this.lastMovement = dx != 0 ? SHIFT : FALL;
        this.x += dx;
        this.y += dy;
    }
}

package theStacker.stacking;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.ImageMaster;
import theStacker.util.TextureLoader;

public class MatrixRenderer {
    private final Texture matrixTex = TextureLoader.getTexture(
        "theStackerResources/images/matrix/matrix.png"
    );
    private final Texture minoTex = TextureLoader.getTexture(
        "theStackerResources/images/matrix/mino.png"
    );
    private final Texture topoutIndicatorTex = TextureLoader.getTexture(
        "theStackerResources/images/matrix/topout_indicator.png"
    );
    private final Texture garbagebarTex = TextureLoader.getTexture(
        "theStackerResources/images/matrix/garbagebar.png"
    );

    public void render(SpriteBatch spriteBatch, Matrix matrix) {
        final Matrix4 saved = spriteBatch.getTransformMatrix().cpy();
        spriteBatch.setColor(Color.WHITE);

        translate(spriteBatch, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        this.drawCentered(spriteBatch, matrixTex, 0, 0, 1f);

        // line up with lower left of centered grid
        translate(spriteBatch, -matrixTex.getWidth() / 2, -matrixTex.getHeight() / 2);
        int garbage = matrix.getIncomingGarbage();
        int height = matrix.board.getStackHeight();
        boolean danger = (garbage + height) > 20;
        Color col = danger
            ? System.currentTimeMillis() % 1000 > 500
            ? Color.ORANGE
            : Color.RED
            : Color.WHITE;
        spriteBatch.setColor(col);
        drawGarbageBar(spriteBatch, matrix);

        // Draw from bottom left of sprite
        spriteBatch.setColor(Color.WHITE);
        translate(spriteBatch, minoTex.getWidth() / 2, minoTex.getHeight() / 2);
        drawBoardBlocks(spriteBatch, matrix.board);
        if (matrix.piece != null) {
            drawPiece(spriteBatch, matrix.piece);
            spriteBatch.setColor(1f, 1f, 1f, 0.5f);
            drawPiece(spriteBatch, matrix.piece.getGhost(matrix.board));
        }

        Piece nextPiece = matrix.getPieceForHeldCard();
        if (nextPiece == null)
            nextPiece = matrix.queue.peek();
        if (nextPiece != null)
            drawPiece(spriteBatch, nextPiece, topoutIndicatorTex, Color.RED);

        if (matrix.hold != null) {
            matrix.hold.y = 20;
            matrix.hold.x = -4;
            drawPiece(spriteBatch, matrix.hold, minoTex, matrix.held ? Color.GRAY : null);
        }

        int previews = 5;
        int y = 20;
        for (Piece piece: matrix.queue) {
            piece.x = 10;
            piece.y = y;
            y -= 4;
            drawPiece(spriteBatch, piece);
            if (previews-- == 0) break;
        }

        spriteBatch.setColor(Color.WHITE);
        spriteBatch.setTransformMatrix(saved);
        translate(spriteBatch, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        translate(spriteBatch, matrixTex.getWidth()/2, matrixTex.getHeight()/2);
        if (matrix.queue.size() > 0)
            translate(spriteBatch, 4 * minoTex.getWidth(), 0);
        scale(spriteBatch, 0.5f);
        drawAttackValues(spriteBatch, matrix);

        spriteBatch.setColor(Color.WHITE);
        spriteBatch.setTransformMatrix(saved);
    }

    private void drawGarbageBar(SpriteBatch spriteBatch, Matrix matrix) {
        spriteBatch.draw(
            garbagebarTex,
            -garbagebarTex.getWidth(),
            0,
            garbagebarTex.getWidth()/2f,
            0,
            garbagebarTex.getWidth(),
            garbagebarTex.getHeight(),
            1,
            matrix.getIncomingGarbage(),
            0.0F,
            0,
            0,
            garbagebarTex.getWidth(),
            garbagebarTex.getHeight(),
            false,
            false
        );
    }

    private void drawAttackValues(SpriteBatch spriteBatch, Matrix matrix) { // adapted from mintyspire's damage patches
        for (int i = 0; i < 4; i++) {
            int dmg = matrix.getAttackPower(i+1);
            drawCentered(spriteBatch, getAttackIntent(dmg), 15, i*-45, 1f);
            FontHelper.damageNumberFont.getData().setScale(1f);
            FontHelper.renderFont(
                spriteBatch,
                FontHelper.damageNumberFont,
                String.format("%s lines: %s", i+1, dmg),
                60,
                i*-45+15,
                Color.SALMON
            );
        }

    }

    private Texture getAttackIntent(int dmg) { // from mintyspire
        if (dmg < 5)
            return ImageMaster.INTENT_ATK_1;
        if (dmg < 10)
            return ImageMaster.INTENT_ATK_2;
        if (dmg < 15)
            return ImageMaster.INTENT_ATK_3;
        if (dmg < 20)
            return ImageMaster.INTENT_ATK_4;
        if (dmg < 25)
            return ImageMaster.INTENT_ATK_5;
        if (dmg < 30)
            return ImageMaster.INTENT_ATK_6;
        return ImageMaster.INTENT_ATK_7;
    }

    private void drawBoardBlocks(SpriteBatch spriteBatch, Board board) {
        for (int y = 0; y < board.getRows(); y++) {
            for (int x = 0; x < Math.min(board.getCols(), 24); x++) {
                if (board.getMino(y, x).isFilled()) {
                    spriteBatch.setColor(board.getMino(y,x).color);
                    drawCentered(spriteBatch, minoTex, x * 20, y * 20, 1f);
                }
            }
        }
    }

    private void drawPiece(SpriteBatch spriteBatch, Piece piece) {
        this.drawPiece(spriteBatch, piece, minoTex, null);
    }
    private void drawPiece(SpriteBatch spriteBatch, Piece piece, Texture tex, Color colorOverride) {
        for (int y = 0; y < piece.getRows(); y++) {
            for (int x = 0; x < piece.getCols(); x++) {
                Mino mino = piece.getMino(y, x);
                if (!mino.isFilled()) continue;
                spriteBatch.setColor(colorOverride != null ? colorOverride : mino.color);
                drawCentered(spriteBatch, tex, (x + piece.x) * 20, (y + piece.y) * 20, 1f);
            }
        }
    }

    private void scale(SpriteBatch spriteBatch, float scale) {
        spriteBatch.setTransformMatrix(spriteBatch.getTransformMatrix().scale(scale, scale, 1));
    }

    private void translate(SpriteBatch spriteBatch, int x, int y) {
        spriteBatch.setTransformMatrix(spriteBatch.getTransformMatrix().translate(x, y, 0));
    }

    private void drawCentered(SpriteBatch spriteBatch, Texture tex, int x, int y, float scale) {
        spriteBatch.draw(
            tex,
            -tex.getWidth()/2f + x,
            -tex.getHeight()/2f + y,
            tex.getWidth()/2f,
            tex.getHeight()/2f,
            tex.getWidth(),
            tex.getHeight(),
            scale,
            scale,
            0.0F,
            0,
            0,
            tex.getWidth(),
            tex.getHeight(),
            false,
            false
        );
    }
}

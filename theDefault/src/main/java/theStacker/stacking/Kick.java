package theStacker.stacking;

public class Kick {
    public final int from;
    public final int to;
    public final int[][] tests;

    public Kick(int from, int to, int[][] tests) {
        this.from = from;
        this.to = to;
        this.tests = tests;
    }
}

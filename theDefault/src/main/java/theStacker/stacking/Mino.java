package theStacker.stacking;

import com.badlogic.gdx.graphics.Color;

import java.util.function.BiConsumer;

import static theStacker.TheStackerMod.logger;

public class Mino {
    private boolean filled;
    public boolean unclearable;
    /**
     * The color of the mino to display, if filled.
     */
    public Color color;
    /**
     * A callback run when the mino is removed or replaced from the board.
     *
     * Honestly this system *incredibly* fragile and should just be replaced with a way of
     * attaching values to minos that copy over with the color. Or better yet get rid of the
     * dumb pooling and treat minos like actual objects.
     */
    public BiConsumer<Board, Mino> onClear;

    public static final Mino GARBAGE = new Mino(true, Color.WHITE, false, null).readonly();
    public static final Mino EMPTY = new Mino(false, Color.WHITE, false, null).readonly();
    public static final Mino OUT_OF_BOUNDS = new Mino(true, Color.BLACK, true, null).readonly();
    public static final Mino DARK_GARBAGE = new Mino(true, Color.BLACK, true, null).readonly();
    public static final Mino I = new Mino(true, new Color(0x00FFFFFF)).readonly();
    public static final Mino J = new Mino(true, new Color(0x0000FFFF)).readonly();
    public static final Mino L = new Mino(true, new Color(0xFFA500FF)).readonly();
    public static final Mino O = new Mino(true, new Color(0xFFFF00FF)).readonly();
    public static final Mino Z = new Mino(true, new Color(0xFF0000FF)).readonly();
    public static final Mino T = new Mino(true, new Color(0xFF00FFFF)).readonly();
    public static final Mino S = new Mino(true, new Color(0x00FF00FF)).readonly();

    public static Mino get(Piece.PieceType pieceType) {
        if (pieceType == null) return EMPTY;
        switch (pieceType) {
            case I: return Mino.I;
            case J: return Mino.J;
            case L: return Mino.L;
            case O: return Mino.O;
            case Z: return Mino.Z;
            case T: return Mino.T;
            case S: return Mino.S;
        }
        throw new RuntimeException("What is a(n) " + pieceType + " piece?");
    }

    public Mino() {
        this(false, Color.WHITE);
    }

    public Mino(boolean filled, Color color) {
        this(filled, color, false, null);
    }

    public Mino(boolean filled, Color color, boolean unclearable, BiConsumer<Board, Mino> onClear) {
        this.filled = filled;
        this.color = color;
        this.unclearable = unclearable;
        this.onClear = onClear;
    }

    private boolean readonly = false;
    public boolean isReadOnly() {
        return readonly;
    }
    private Mino readonly() {
        this.readonly = false;
        return this;
    }

    /**
     * Sets the mino to some other mino's value
     * @param board The board this mino is on. If null, skips calling onClear callbacks
     * @param other The source mino to copy values from
     */
    public void set(Board board, Mino other) {
        if (this.readonly) {
            logger.warn("Attempt to set readonly mino");
            return;
        }
        if (board != null && this.onClear != null)
            this.onClear.accept(board, this);
        this.filled = other.filled;
        this.color = other.color;
        this.onClear = other.onClear;
        this.unclearable = other.unclearable;
    }

    /**
     * Sets the mino *without* calling its onClear callback
     */
    public void reset() {
        if (this.readonly) {
            logger.warn("Attempt to reset readonly mino");
            return;
        }
        this.filled = false;
        this.color = Color.WHITE;
        this.onClear = null;
        this.unclearable = false;
    }

    /**
     * Whether the mino is filled. A filled mino is rendered as solid and can be
     * copied on to another board. An unfilled mino is just an object placeholder.
     */
    public boolean isFilled() {
        return filled;
    }
}

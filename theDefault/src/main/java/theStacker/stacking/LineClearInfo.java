package theStacker.stacking;

public class LineClearInfo {
    public final Matrix matrix;
    public final Piece piece;
    public final int lines;
    public final int combo;
    public final boolean tspin;
    public final boolean mini;
    public final boolean perfectclear;
    public final int b2b;

    public LineClearInfo(Matrix matrix, Piece piece, int lines, int combo, int b2b, boolean tspin, boolean mini, boolean perfectclear) {
        this.matrix = matrix;
        this.piece = piece;
        this.lines = lines;
        this.combo = combo;
        this.b2b = b2b;
        this.tspin = tspin;
        this.mini = mini;
        this.perfectclear = perfectclear;
    }
}

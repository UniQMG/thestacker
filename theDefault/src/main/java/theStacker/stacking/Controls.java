package theStacker.stacking;

import com.badlogic.gdx.Gdx;
import theStacker.util.Mutable;

import java.util.EnumMap;
import java.util.Map;

import static theStacker.stacking.Controls.DASSide.*;
import static theStacker.stacking.Controls.GameKey.*;
import static theStacker.stacking.Piece.RotateDirection.*;

public class Controls {
    public enum GameKey {
        MOVE_LEFT,
        MOVE_RIGHT,
        SOFT_DROP,
        HARD_DROP,
        ROTATE_CCW,
        ROTATE_CW,
        ROTATE_180,
        HOLD
    }
    /* used in a callback */
    public enum ControlEvent { CANCEL }
    public enum DASSide {
        NONE, LEFT, RIGHT
    }

    public int DAS = 10;
    public int ARR = 2;
    public static final int SDF = 20;
    public static final int LOCK_DELAY = 30;
    public static final int LD_RESETS = 15;
    private final Matrix matrix;

    private Map<GameKey, Boolean> lastKeys = new EnumMap<>(GameKey.class);

    private int DAS_left = 0;
    private int DAS_right = 0;
    private int ARR_left = 0;
    private int ARR_right = 0;
    private DASSide activeSide = NONE;

    public Controls(Matrix matrix) {
        this.matrix = matrix;
        for (GameKey key: GameKey.values())
            lastKeys.put(key, false);
    }

    private boolean keyDown(GameKey key) {
        return keyDown(key, true);
    }
    private boolean keyDown(GameKey key, boolean fireEvents) {
        if (Gdx.input.isKeyPressed(matrix.keymap.get(key))) {
            if (!fireEvents) return true;
            Mutable<Boolean> allow = new Mutable<>(true);
            matrix.input.get(key).fire(controlEvent -> allow.set(false));
            return allow.get();
        }
        return false;
    }
    private boolean keyJustDown(GameKey key) {
        return !lastKeys.get(key) && keyDown(key);
    }

    void frame() {
        if (keyJustDown(HOLD) && !matrix.held && matrix.enableHold) {
            matrix.held = true;
            Piece temp = matrix.hold;
            matrix.hold = matrix.piece;
            matrix.piece = temp;
            if (matrix.piece == null)
                matrix.nextPiece();
            if (matrix.piece != null)
                matrix.piece.reposition();
        }

        if (matrix.piece == null) return;

        if (keyJustDown(MOVE_LEFT))
            tryMove(-1, 0);
        if (!keyDown(MOVE_LEFT)) {
            if (activeSide == LEFT) activeSide = NONE;
            DAS_left = 0;
            ARR_left = 0;
        } else if (DAS_left < DAS) {
            if (activeSide == NONE) activeSide = LEFT;
            DAS_left++;
            if (DAS_left >= DAS)
                tryMove(-1, 0);
        } else {
            ARR_left++;
            while (ARR_left > ARR) {
                ARR_left -= ARR;
                if (!tryMove(-1, 0))
                    ARR_left = 0;
            }
        }

        if (keyJustDown(MOVE_RIGHT))
            tryMove(1, 0);
        if (!keyDown(MOVE_RIGHT)) {
            if (activeSide == RIGHT) activeSide = NONE;
            DAS_right = 0;
            ARR_right = 0;
        } else if (DAS_right < DAS) {
            if (activeSide == NONE) activeSide = RIGHT;
            DAS_right++;
            if (DAS_right >= DAS)
                tryMove(1, 0);
        } else {
            ARR_right++;
            while (ARR_right > ARR) {
                ARR_right -= ARR;
                if (!tryMove(1, 0))
                    ARR_right = 0;
            }
        }


        if (keyJustDown(ROTATE_CCW) && matrix.piece.canRotate(matrix.board, CCW))
            matrix.piece.rotate(matrix.board, CCW);

        if (keyJustDown(ROTATE_CW) && matrix.piece.canRotate(matrix.board, CW))
            matrix.piece.rotate(matrix.board, CW);

        if (keyJustDown(ROTATE_180) && matrix.piece.canRotate(matrix.board, R180))
            matrix.piece.rotate(matrix.board, R180);

        matrix.piece.fallProgress += matrix.gravity;
        if (keyDown(SOFT_DROP))
            matrix.piece.fallProgress += matrix.gravity * (SDF-1);
        if (keyJustDown(HARD_DROP))
            matrix.piece.fallProgress = Integer.MAX_VALUE;
        while (matrix.piece.fallProgress > 60) {
            matrix.piece.fallProgress -= 60;
            if (!tryMove(0, -1))
                matrix.piece.fallProgress = 0;
        }

        boolean resting = !matrix.piece.canMove(matrix.board, 0, -1);
        if (resting) {
            matrix.piece.lockProgress++;
            if (matrix.piece.lockProgress > LOCK_DELAY)
                matrix.lockPiece();
        }

        if (keyJustDown(HARD_DROP))
            matrix.lockPiece();

        for (GameKey key: GameKey.values())
            lastKeys.put(key, keyDown(key, false));
    }

    private void tryResetLockDelay() {
        if (matrix.piece.lockProgress > 0 && matrix.piece.lockResets < LD_RESETS) {
            matrix.piece.lockProgress = 0;
            matrix.piece.lockResets++;
        }
    }

    private boolean tryMove(int dx, int dy) {
        if (matrix.piece.canMove(matrix.board, dx, dy)) {
            this.tryResetLockDelay();
            matrix.piece.move(dx, dy);
            return true;
        }
        return false;
    }

}

package theStacker.cards.attack;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.actions.AddRandomPieceToHandAction;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;

import static theStacker.TheStackerMod.makeCardPath;

public class TacticalBrick extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(TacticalBrick.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public TacticalBrick() {
        super(ID, IMG, 2, CardType.ATTACK, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.UNCOMMON, CardTarget.ENEMY);
        baseDamage = 12;
        damage = 12;
        baseMagicNumber = 6;
        magicNumber = 6;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        final int initialHealth = monster.currentHealth;

        addToBot(new DamageAction(
            monster,
            new DamageInfo(player, damage, damageTypeForTurn),
            AbstractGameAction.AttackEffect.BLUNT_LIGHT
        ) {
            @Override
            public void update() {
                super.update();
                if (this.isDone) {
                    int unblocked = initialHealth - monster.currentHealth;
                    addToBot(new AddRandomPieceToHandAction(unblocked / magicNumber));
                }
            }
        });
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(-2);
            initializeDescription();
        }
    }
}

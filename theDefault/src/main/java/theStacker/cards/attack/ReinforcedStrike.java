package theStacker.cards.attack;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.cards.status.PieceCard;
import theStacker.characters.TheStacker;

import static theStacker.TheStackerMod.makeCardPath;

public class ReinforcedStrike extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(ReinforcedStrike.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public ReinforcedStrike() {
        super(ID, IMG, 1, CardType.ATTACK, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.COMMON, CardTarget.ENEMY);
        this.baseDamage = 5;
        this.damage = 5;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        int size = AbstractDungeon.actionManager.cardsPlayedThisTurn.size();
        for (int i = 0; i < size; i++) {
            addToBot(new DamageAction(
                monster,
                new DamageInfo(player, damage, damageTypeForTurn),
                AbstractGameAction.AttackEffect.SLASH_DIAGONAL
            ));
        }
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeDamage(2);
            initializeDescription();
        }
    }
}

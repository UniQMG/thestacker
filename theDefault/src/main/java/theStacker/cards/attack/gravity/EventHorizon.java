package theStacker.cards.attack.gravity;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.GravityPower;

import static theStacker.TheStackerMod.makeCardPath;

public class EventHorizon extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(EventHorizon.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public EventHorizon() {
        super(ID, IMG, 3, CardType.ATTACK, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.RARE, CardTarget.ENEMY);
        this.baseMagicNumber = 25;
        this.magicNumber = 25;
    }

    @Override
    public boolean canPlay(AbstractCard card) {
        if (!AbstractDungeon.player.hasPower(GravityPower.POWER_ID)) return false;
        return AbstractDungeon.player.getPower(GravityPower.POWER_ID).amount >= magicNumber;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        this.addToBot(new DamageAction(
            monster,
            new DamageInfo(player, damage, damageTypeForTurn),
            AbstractGameAction.AttackEffect.BLUNT_HEAVY
        ));
        this.addToBot(new RemoveSpecificPowerAction(player, player, GravityPower.POWER_ID));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(-5);
            initializeDescription();
        }
    }
}

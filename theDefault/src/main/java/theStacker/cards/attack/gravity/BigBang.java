package theStacker.cards.attack.gravity;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.GravityPower;
import theStacker.util.Helpers;

import static theStacker.TheStackerMod.makeCardPath;

public class BigBang extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(BigBang.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public BigBang() {
        super(ID, IMG, 1, CardType.ATTACK, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.UNCOMMON, CardTarget.ENEMY);
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        int power = Helpers.powerStacks(player, GravityPower.POWER_ID);
        if (power == 0) return;
        addToBot(new RemoveSpecificPowerAction(player, player, GravityPower.POWER_ID));
        addToBot(new DamageAction(
            monster,
            new DamageInfo(player, power, damageTypeForTurn),
            AbstractGameAction.AttackEffect.BLUNT_LIGHT
        ));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            // upgradeBaseCost(1-1);
            // rawDescription = cardStrings.UPGRADE_DESCRIPTION;
            initializeDescription();
        }
    }
}

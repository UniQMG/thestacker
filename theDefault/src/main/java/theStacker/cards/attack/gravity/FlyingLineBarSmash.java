package theStacker.cards.attack.gravity;

import com.badlogic.gdx.graphics.Color;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.GravityPower;
import theStacker.util.Helpers;

import static theStacker.TheStackerMod.makeCardPath;

public class FlyingLineBarSmash extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(FlyingLineBarSmash.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public FlyingLineBarSmash() {
        super(ID, IMG, 1, CardType.ATTACK, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.COMMON, CardTarget.ENEMY);
        this.baseDamage = 8;
        this.damage = 8;
        this.baseMagicNumber = 1;
        this.magicNumber = 1;
    }

    private static Color noglow = new Color(0x33e5ff3f);
    @Override
    public void triggerOnGlowCheck() {
        int gravity = Helpers.powerStacks(AbstractDungeon.player, GravityPower.POWER_ID);
        this.glowColor = gravity > 5 ? AbstractCard.GOLD_BORDER_GLOW_COLOR : noglow;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        if (Helpers.powerStacks(player, GravityPower.POWER_ID) > 5) {
            addToBot(new ApplyPowerAction(
                monster,
                player,
                new VulnerablePower(monster, magicNumber, false)
            ));
        }
        addToBot(new DamageAction(
            monster,
            new DamageInfo(player, damage, damageTypeForTurn),
            AbstractGameAction.AttackEffect.NONE
        ));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeDamage(4);
            upgradeMagicNumber(1);
            initializeDescription();
        }
    }
}

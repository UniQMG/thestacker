package theStacker.cards.attack;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.cards.status.PieceCard;
import theStacker.characters.TheStacker;
import theStacker.stacking.Piece;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static theStacker.TheStackerMod.makeCardPath;

public class BackToBack extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(BackToBack.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public BackToBack() {
        super(ID, IMG, -1, CardType.ATTACK, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.UNCOMMON, CardTarget.ENEMY);
        isMultiDamage = true;
        baseDamage = 15;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new AbstractGameAction() {
            @Override
            public void update() {
                int effect = player.energy.energy;
                if (!freeToPlayOnce)
                    player.energy.use(effect);

                if (player.hasRelic("Chemical X")) {
                    player.getRelic("Chemical X").flash();
                    effect += 2;
                }

                AbstractCard[] cards = (AbstractCard[]) player.hand.group.stream().filter(card -> {
                    if (!(card instanceof PieceCard)) return false;
                    switch (((PieceCard) card).getPiece()) {
                        case T: case I:
                            return true;
                        default:
                            return false;
                    }
                }).toArray();

                for (int i = 0; i < Math.min(effect-1, cards.length); i++) {
                    player.hand.moveToExhaustPile(cards[i]);

                    ArrayList<AttackEffect> rand = new ArrayList<>();
                    rand.add(AttackEffect.SLASH_DIAGONAL);
                    rand.add(AttackEffect.SLASH_HORIZONTAL);
                    rand.add(AttackEffect.SLASH_VERTICAL);
                    rand.add(AttackEffect.SLASH_HEAVY);

                    addToBot(new DamageAction(
                        monster,
                        new DamageInfo(player, damage, damageTypeForTurn),
                        rand.get((int) (Math.random() * rand.size()))
                    ));
                }
            }
        });
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeDamage(5);
            initializeDescription();
        }
    }
}

package theStacker.cards.attack.opener;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.stacking.Matrix;
import theStacker.stacking.Piece;

import static com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE;
import static theStacker.TheStackerMod.makeCardPath;
import static theStacker.stacking.Piece.PieceType.*;

public class PC extends Opener {
    public static final String IMG = makeCardPath("placeholder.png");

    public PC() {
        super(PC.class.getSimpleName(), RARE, 1);
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new AbstractGameAction() {
            @Override
            public void update() {
                if (apply(player)) {
                    Matrix matrix = ((MatrixHolder) player).getMatrix();
                    switch ((int) (Math.random() * 4)) {
                        case 0:
                            matrix.queuePiece(Piece.get(L));
                            matrix.queuePiece(Piece.get(S));
                            matrix.queuePiece(Piece.get(T));
                            break;

                        case 1:
                            matrix.queuePiece(Piece.get(T));
                            matrix.queuePiece(Piece.get(J));
                            matrix.queuePiece(Piece.get(L));
                            break;

                        case 2:
                            matrix.queuePiece(Piece.get(T));
                            matrix.queuePiece(Piece.get(O));
                            matrix.queuePiece(Piece.get(I));
                            break;

                        case 3:
                            matrix.queuePiece(Piece.get(Z));
                            matrix.queuePiece(Piece.get(J));
                            matrix.queuePiece(Piece.get(I));
                            break;
                    }
                }
                this.isDone = true;
            }
        });
    }

    @Override
    public AbstractCard makeCopy() {
        return new PC();
    }

    @Override
    public boolean canPlay(AbstractCard card) {
        if (!(card instanceof PC)) return true;
        return AbstractDungeon.actionManager.cardsPlayedThisCombat.size() == 0;
    }

    @Override
    public Piece.PieceType[][] build() {
        return new Piece.PieceType[][] {
            { L, L, L, I, null, null, null, null, S, S },
            { L, O, O, I, null, null, null,    S, S, T },
            { J, O, O, I, null, null,    Z,    Z, T, T },
            { J, J, J, I, null, null, null,    Z, Z, T }
        };
    }
}

package theStacker.cards.attack.opener;

import basemod.AutoAdd;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import theStacker.MatrixHolder;
import theStacker.cards.AbstractDynamicCard;
import theStacker.cards.status.PieceCard;
import theStacker.characters.TheStacker;
import theStacker.stacking.Matrix;
import theStacker.stacking.Mino;
import theStacker.stacking.Piece;

import static theStacker.TheStackerMod.makeCardPath;
import static theStacker.TheStackerMod.makeID;

@AutoAdd.Ignore
public abstract class Opener extends AbstractDynamicCard {
    public static final String IMG = makeCardPath("placeholder.png");
    public static final CardColor COLOR = TheStacker.Enums.STACKER_CYAN_COLOR;
    private final CardStrings cardStrings;

    public Opener(String id, CardRarity rarity, int cost) {
        super(
            makeID("Opener" + id),
            // Replace this based on opener ID once we start actually making art
            makeCardPath("placeholder.png"),
            cost,
            CardType.ATTACK,
            TheStacker.Enums.STACKER_CYAN_COLOR,
            rarity,
            CardTarget.NONE
        );
        this.cardStrings = CardCrawlGame.languagePack.getCardStrings(this.cardID);
    }

    public boolean apply(AbstractPlayer player) {
        long cardsPlayed = AbstractDungeon
            .actionManager
            .cardsPlayedThisCombat
            .parallelStream()
            .filter(card -> !(card instanceof PieceCard))
            .count();
        if (cardsPlayed > 1) return false;
        if (!(player instanceof MatrixHolder)) return false;

        Matrix matrix = ((MatrixHolder) player).getMatrix();
        Piece.PieceType[][] pieces = build();
        int yOffset = matrix.board.getStackHeight();
        for (int y = yOffset; y < yOffset + pieces.length; y++) {
            int ly = pieces.length - (y - yOffset) - 1;
            for (int x = 0; x < pieces[ly].length; x++) {
                matrix.board.getMino(y, x).set(matrix.board, Mino.get(pieces[ly][x]));
            }
        }

        return true;
    }

    abstract Piece.PieceType[][] build();

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            this.isInnate = true;
            rawDescription = cardStrings.UPGRADE_DESCRIPTION;
            initializeDescription();
        }
    }
}

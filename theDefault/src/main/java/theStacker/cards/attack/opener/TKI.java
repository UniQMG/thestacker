package theStacker.cards.attack.opener;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.stacking.Piece;

import static com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON;
import static theStacker.TheStackerMod.makeCardPath;
import static theStacker.stacking.Piece.PieceType.*;

public class TKI extends Opener {
    public static final String IMG = makeCardPath("placeholder.png");

    public TKI() {
        super(TKI.class.getSimpleName(), UNCOMMON, 1);
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new AbstractGameAction() {
            @Override
            public void update() {
                if (apply(player)) ((MatrixHolder) player).getMatrix().queuePiece(Piece.get(T));
                this.isDone = true;
            }
        });
    }

    @Override
    public AbstractCard makeCopy() {
        return new TKI();
    }

    @Override
    public boolean canPlay(AbstractCard card) {
        if (!(card instanceof TKI)) return true;
        return AbstractDungeon.actionManager.cardsPlayedThisCombat.size() == 0;
    }

    @Override
    public Piece.PieceType[][] build() {
        return new Piece.PieceType[][] {
            { null, null, null, null, null, null, null, J, null, null },
            {    L, null, null,    Z,    Z, null,    S, J,    J,    J },
            {    L, null, null, null,    Z,    Z,    S, S,    O,    O },
            {    L,    L, null,    I,    I,    I,    I, S,    O,    O }
        };
    }
}

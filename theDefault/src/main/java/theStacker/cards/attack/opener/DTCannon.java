package theStacker.cards.attack.opener;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.stacking.Matrix;
import theStacker.stacking.Piece;

import static com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON;
import static theStacker.TheStackerMod.makeCardPath;
import static theStacker.stacking.Piece.PieceType.*;

public class DTCannon extends Opener {
    public static final String IMG = makeCardPath("placeholder.png");

    public DTCannon() {
        super(DTCannon.class.getSimpleName(), UNCOMMON, 1);
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new AbstractGameAction() {
            @Override
            public void update() {
                if (apply(player)) {
                    Matrix matrix = ((MatrixHolder) player).getMatrix();
                    matrix.queuePiece(Piece.get(T));
                    matrix.queuePiece(Piece.get(T));
                }
                this.isDone = true;
            }
        });
    }

    @Override
    public AbstractCard makeCopy() {
        return new DTCannon();
    }

    @Override
    public boolean canPlay(AbstractCard card) {
        if (!(card instanceof DTCannon)) return true;
        return AbstractDungeon.actionManager.cardsPlayedThisCombat.size() == 0;
    }

    @Override
    @SuppressWarnings({"ConstantConditions", "JavacQuirks"})
    public Piece.PieceType[][] build() {
        Piece.PieceType _ = null;
        return new Piece.PieceType[][] {
            { _, _, L, L, _, _, _, _, S, S },
            { _, _, _, L, Z, Z, _, S, S, I },
            { J, J, _, L, T, Z, Z, O, O, I },
            { J, _, _, T, T, T, I, O, O, I },
            { J, _, _, _, S, S, I, Z, Z, I },
            { O, O, _, S, S, L, I, J, Z, Z },
            { O, O, _, L, L, L, I, J, J, J }
        };
    }
}

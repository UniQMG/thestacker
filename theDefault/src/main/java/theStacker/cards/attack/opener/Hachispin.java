package theStacker.cards.attack.opener;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.stacking.Matrix;
import theStacker.stacking.Piece;

import static com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE;
import static theStacker.TheStackerMod.makeCardPath;
import static theStacker.stacking.Piece.PieceType.*;

public class Hachispin extends Opener {
    public static final String IMG = makeCardPath("placeholder.png");

    public Hachispin() {
        super(Hachispin.class.getSimpleName(), RARE, 1);
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new AbstractGameAction() {
            @Override
            public void update() {
                if (apply(player)) {
                    Matrix matrix = ((MatrixHolder) player).getMatrix();
                    matrix.queuePiece(Piece.get(T)); // tss

                    matrix.queuePiece(Piece.get(Z)); // tst
                    matrix.queuePiece(Piece.get(J));
                    matrix.queuePiece(Piece.get(L));
                    matrix.queuePiece(Piece.get(I));
                    matrix.queuePiece(Piece.get(O));
                    matrix.queuePiece(Piece.get(S));
                    matrix.queuePiece(Piece.get(T));

                    matrix.queuePiece(Piece.get(Z)); // pc
                    matrix.queuePiece(Piece.get(T));
                    matrix.queuePiece(Piece.get(S));
                    matrix.queuePiece(Piece.get(J));
                    matrix.queuePiece(Piece.get(L));
                    matrix.queuePiece(Piece.get(O));
                }
                this.isDone = true;
            }
        });
    }

    @Override
    public AbstractCard makeCopy() {
        return new Hachispin();
    }

    @Override
    public boolean canPlay(AbstractCard card) {
        if (!(card instanceof Hachispin)) return true;
        return AbstractDungeon.actionManager.cardsPlayedThisCombat.size() == 0;
    }

    @Override
    @SuppressWarnings({"ConstantConditions", "JavacQuirks"})
    public Piece.PieceType[][] build() {
        Piece.PieceType _ = null;
        return new Piece.PieceType[][] {
            { _, _, _, _, _, _, Z, _, _, _ },
            { I, _, _, _, _, Z, Z, _, _, _ },
            { I, _, J, J, J, Z, _, _, _, _ },
            { I, L, L, L, J, O, O, _, S, S },
            { I, L, _, _, _, O, O, S, S, _ }
        };
    }
}

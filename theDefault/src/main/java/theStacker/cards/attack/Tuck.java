package theStacker.cards.attack;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.actions.QueueRandomPieceAction;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;

import static theStacker.TheStackerMod.makeCardPath;

public class Tuck extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Tuck.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public Tuck() {
        super(ID, IMG, 2, CardType.ATTACK, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.COMMON, CardTarget.ENEMY);
        this.baseMagicNumber = 1;
        this.magicNumber = 1;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new ApplyPowerAction(
            monster,
            player,
            new VulnerablePower(monster, this.magicNumber, false),
            this.magicNumber
        ));
        addToBot(new QueueRandomPieceAction(this.magicNumber));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(1);
            initializeDescription();
        }
    }
}

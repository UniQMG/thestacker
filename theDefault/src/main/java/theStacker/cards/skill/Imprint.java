package theStacker.cards.skill;

import basemod.BaseMod;
import basemod.abstracts.CustomSavableRaw;
import com.evacipated.cardcrawl.mod.stslib.variables.ExhaustiveVariable;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.GetAllInBattleInstances;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.stacking.Board;

import java.util.ArrayList;
import java.util.Iterator;

import static theStacker.TheStackerMod.makeCardPath;

public class Imprint extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Imprint.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);
    private Board board;

    public Imprint() {
        super(ID, IMG, 1, CardType.SKILL, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.UNCOMMON, CardTarget.NONE);
        this.exhaust = true;
        ExhaustiveVariable.setBaseValue(this, 1);
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new AbstractGameAction() {
            @Override
            public void update() {
                if (!(player instanceof MatrixHolder)) {
                    this.isDone = true;
                    return;
                }

                if (board == null) {
                    board = ((MatrixHolder) player).getMatrix().board.copy();
                    for (int y = 0; y < board.getRows(); y++)
                        for (int x = 0; x < board.getCols(); x++)
                            board.getMino(y, x).onClear = null;
                } else {
                    ((MatrixHolder) player).getMatrix().board = board.copy();
                    board = null;
                }

                for (AbstractCard card : AbstractDungeon.player.masterDeck.group) {
                    if (!card.uuid.equals(uuid)) continue;
                    ((Imprint) card).board = board;
                    card.initializeDescription();
                }

                this.isDone = true;
                initializeDescription();
            }
        });
    }

    @Override
    public void initializeDescription() {
        this.rawDescription = ((
            upgraded
                ? cardStrings.DESCRIPTION
                : cardStrings.UPGRADE_DESCRIPTION
        ) + '(' + (
            board == null
                ? cardStrings.EXTENDED_DESCRIPTION[0]
                : board.getStackHeight() + cardStrings.EXTENDED_DESCRIPTION[1]
        ) + ')');
        super.initializeDescription();
    }

    @Override
    public AbstractCard makeCopy() {
        return new Imprint();
    }

    @Override
    public AbstractCard makeStatEquivalentCopy() {
        Imprint card = (Imprint) super.makeStatEquivalentCopy();
        card.board = this.board;
        return card;
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            ExhaustiveVariable.upgrade(this, 2);
            rawDescription = cardStrings.UPGRADE_DESCRIPTION;
            initializeDescription();
        }
    }

    static { BaseMod.addSaveField("Board", new BoardSaver()); }
    private static class BoardSaver implements CustomSavableRaw {
        // modified example from jorb's wanderer trilogy (MIT)
        // https://github.com/dbjorge/jorbs-spire-mod/blob/f03b50be2c33b82a0f05685b31c4088a54ea46bd/src/main/java/stsjorbsmod/cards/CardSaveData.java

        @Override
        public JsonElement onSaveRaw() {
            Gson gson = new Gson();

            ArrayList<Board> boards = new ArrayList<>();
            for (AbstractCard card: AbstractDungeon.player.masterDeck.group) {
                if (card instanceof Imprint) {
                    boards.add(((Imprint) card).board);
                } else {
                    boards.add(null);
                }
            }

            return gson.toJsonTree(boards);
        }

        @Override
        public void onLoadRaw(JsonElement jsonElement) {
            ArrayList<Board> boards;
            try  {
                Gson gson = new Gson();
                boards = gson.fromJson(jsonElement, new TypeToken<ArrayList<Board>>(){}.getType());
            } catch(Exception ex) {
                boards = new ArrayList<>();
                ex.printStackTrace();
            }

            CardGroup deck = AbstractDungeon.player.masterDeck;
            for (int i = 0; i < Math.min(deck.size(), boards.size()); i++) {
                AbstractCard card = deck.group.get(i);
                Board board = boards.get(i);
                if (card instanceof Imprint)
                    ((Imprint) card).board = board;
            }
        }
    }
}

package theStacker.cards.skill.pieceGen;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.actions.AddRandomPieceToHandAction;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;

import static theStacker.TheStackerMod.makeCardPath;

public class Construct extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Construct.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public Construct() {
        super(ID, IMG, 1, CardType.SKILL, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.COMMON, CardTarget.NONE);
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new AddRandomPieceToHandAction(1));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(0);
            initializeDescription();
        }
    }
}

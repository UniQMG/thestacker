package theStacker.cards.skill;

import com.evacipated.cardcrawl.mod.stslib.fields.cards.AbstractCard.FleetingField;
import com.megacrit.cardcrawl.actions.common.HealAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;

import static theStacker.TheStackerMod.makeCardPath;

public class SecretGrade extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(SecretGrade.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    public SecretGrade() {
        super(ID, IMG, 0, CardType.SKILL, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.SPECIAL, CardTarget.NONE);
        FleetingField.fleeting.set(this, true);
    }

    @Override
    public void triggerWhenDrawn() {
        addToBot(new HealAction(AbstractDungeon.player, AbstractDungeon.player, 1));
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new HealAction(AbstractDungeon.player, AbstractDungeon.player, AbstractDungeon.player.maxHealth));
    }

    @Override
    public boolean canUpgrade() {
        return false;
    }

    @Override
    public void upgrade() {
    }
}

package theStacker.cards.skill;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.MatrixHolder;
import theStacker.characters.TheStacker;
import theStacker.stacking.Board;
import theStacker.stacking.Mino;

import static theStacker.TheStackerMod.makeCardPath;

public class Skim extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Skim.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    private static final CardRarity RARITY = CardRarity.COMMON;
    private static final CardTarget TARGET = CardTarget.NONE;
    private static final CardType TYPE = CardType.SKILL;
    public static final CardColor COLOR = TheStacker.Enums.STACKER_CYAN_COLOR;
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    private static final int COST = 1;
    private static final int UPGRADED_COST = 0;

    public Skim() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        AbstractDungeon.actionManager.addToBottom(new AbstractGameAction() {
            @Override
            public void update() {
                if (player instanceof MatrixHolder) {
                    Board board = ((MatrixHolder) player).getMatrix().board;

                    int y; // Find top filled line
                    find_y: for (y = board.getRows()-2; y > 0; y--)
                        for (int x = 0; x < board.getCols(); x++)
                            if (board.getMino(y, x).isFilled())
                                break find_y;

                    int hole = 0; // Find hole
                    for (int x = 0; x < board.getCols(); x++) {
                        if (!board.getMino(y, x).isFilled()) {
                            hole = x;
                            break;
                        }
                    }

                    // Align holes
                    for (int iy = y; iy > Math.max(y-3, 0); iy--)
                        for (int x = 0; x < board.getCols(); x++)
                            if (x == hole)
                                board.getMino(iy, x).set(board, Mino.EMPTY);
                            else
                                board.getMino(iy, x).set(board, Mino.GARBAGE);
                }
                this.isDone = true;
            }
        });
    }


    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(UPGRADED_COST);
            rawDescription = cardStrings.UPGRADE_DESCRIPTION;
            initializeDescription();
        }
    }
}

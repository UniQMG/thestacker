package theStacker.cards.skill.block;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.stacking.Matrix;

import static theStacker.TheStackerMod.makeCardPath;

public class Palisade extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Palisade.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public Palisade() {
        super(ID, IMG, 2, CardType.SKILL, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.COMMON, CardTarget.NONE);
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        if (player instanceof MatrixHolder) {
            Matrix matrix = ((MatrixHolder) player).getMatrix();
            int height = matrix.board.getStackHeight();
            addToBot(new GainBlockAction(player, height));
        }
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(1);
            initializeDescription();
        }
    }
}

package theStacker.cards.skill;

import com.badlogic.gdx.graphics.Color;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.actions.PickSingleCardAction;
import theStacker.cards.AbstractDynamicCard;
import theStacker.MatrixHolder;
import theStacker.characters.TheStacker;
import theStacker.stacking.*;
import theStacker.util.Mutable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;

import static theStacker.TheStackerMod.makeCardPath;

@SuppressWarnings("unused")
public class StackedQueue extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(StackedQueue.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    private static final CardRarity RARITY = CardRarity.RARE;
    private static final CardTarget TARGET = CardTarget.NONE;
    private static final CardType TYPE = CardType.SKILL;
    public static final CardColor COLOR = TheStacker.Enums.STACKER_CYAN_COLOR;
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    private static final int COST = 1;
    private static final int UPGRADED_COST = 0;

    public StackedQueue() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        if (!(player instanceof MatrixHolder)) return;
        addToBot(new PickSingleCardAction(cardStrings.EXTENDED_DESCRIPTION[0], card -> {
            Matrix matrix = ((MatrixHolder) player).getMatrix();
            player.hand.removeCard(card);

            int x, y;
            int[][] dirs = new int[][] { {-1, 0}, {1, 0}, {0, -1}, {0, 1} };
            int[][] allDirs = new int[][] {
                {-1, -1}, {0, -1}, { 1, -1}, { 1, 0},
                { 1,  1}, {0,  1}, {-1,  1}, {-1, 0},
            };
            ArrayList<Integer[]> list = new ArrayList<>();
            list.add(new Integer[]{0,0});
            ArrayList<Integer[]> unused = new ArrayList<>(list);
            int attempts = 0;

            search: while(list.size() < card.cost+1 && attempts++ < 10000) {
                if (unused.size() == 0) break;
                Integer[] starting = unused.remove((int) (Math.random() * unused.size()));
                x = starting[0];
                y = starting[1];

                ArrayList<Integer[]> dirsLeft = new ArrayList<>();
                for (int[] dir: dirs)
                    dirsLeft.add(new Integer[]{ dir[0], dir[1] });
                Collections.shuffle(dirsLeft);

                for (Integer[] dir : dirsLeft) {
                    int dx = dir[0], dy = dir[1];

                    for (Integer[] position: list)
                        if (position[0] == x+dx && position[1] == y+dy)
                            continue search;

                    int neighbors = 0;
                    findNeighbor: for (int[] delta: allDirs) {
                        for (Integer[] position: list) {
                            if (position[0] == x+dx+delta[0] && position[1] == y+dy+delta[1]) {
                                neighbors++;
                                continue findNeighbor;
                            }
                        }
                    }

                    if (neighbors >= 3 || x > 5 || y > 5 || x < -5 || y < -5)
                        continue;

                    list.add(new Integer[]{ x+dx, y+dy });
                    unused.clear();
                    unused.addAll(list);
                }
            }

            int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
            for (Integer[] pos: list) {
                minX = Math.min(pos[0], minX);
                minY = Math.min(pos[1], minY);
            }
            int maxX = 0, maxY = 0;
            for (Integer[] pos: list) {
                pos[0] -= minX;
                pos[1] -= minY;
                maxX = Math.max(pos[0], maxX);
                maxY = Math.max(pos[1], maxY);
            }
            int size = Math.max(maxX, maxY)+1;

            List<int[]> result = new ArrayList<>();
            int[][] minos = new int[list.size()][2];
            for (int i = 0; i < minos.length; i++) {
                minos[i] = Arrays.stream(list.get(i)).mapToInt(v2 -> v2).toArray();
            }

            TheStackerMod.logger.info("Generating random piece: " + attempts + " attempts made");
            TheStackerMod.logger.info(Arrays.deepToString(minos));
            TheStackerMod.logger.info("Max size: " + size);


            Mutable<Boolean> done = new Mutable<>(false);
            Piece piece = new Piece(null, size, Color.GRAY, minos, new Kick[]{}, new BiConsumer<Board, Mino>() {
                @Override
                public void accept(Board board, Mino mino) {
                    // Execute next tick so the rest of the clears have a chance to go through. Use clear handler
                    // as event ID so the nextTick handler is only called once per StackedQueue piece
                    matrix.nextTick.listen(this, () -> {
                        if (board != matrix.board || done.get())
                            return false;

                        boolean failed = false;
                        for (int y = 0; y < board.getRows(); y++)
                            for (int x = 0; x < board.getCols(); x++)
                                if (board.getMino(y, x).onClear == this)
                                    failed = true;

                        if (failed) {
                            // Didn't clear it all at once
                            player.hand.moveToExhaustPile(card);
                            TheStackerMod.logger.info("Failed to clear piece all at once, exhausted!");
                        } else {
                            // Cleared it all at once!
                            player.hand.addToHand(card);
                            card.freeToPlayOnce = true;
                            TheStackerMod.logger.info("Free piece!");
                        }
                        done.set(true);
                        return false;
                    });
                }
            });
            matrix.queuePiece(piece);
        }));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(UPGRADED_COST);
            initializeDescription();
        }
    }
}

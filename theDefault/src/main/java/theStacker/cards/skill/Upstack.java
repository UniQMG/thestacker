package theStacker.cards.skill;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.MatrixHolder;
import theStacker.characters.TheStacker;
import theStacker.stacking.Board;

import static theStacker.TheStackerMod.makeCardPath;

public class Upstack extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Upstack.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    private static final CardRarity RARITY = CardRarity.COMMON;
    private static final CardTarget TARGET = CardTarget.NONE;
    private static final CardType TYPE = CardType.SKILL;
    public static final CardColor COLOR = TheStacker.Enums.STACKER_CYAN_COLOR;

    private static final int COST = 1;
    private static final int UPGRADED_COST = 0;

    public Upstack() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    }


    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        AbstractDungeon.actionManager.addToBottom(new AbstractGameAction() {
            @Override
            public void update() {
                if (player instanceof MatrixHolder) {
                    Board board = ((MatrixHolder) player).getMatrix().board;

                    int y;
                    find_y: for (y = board.getRows()-2; y > 0; y--)
                        for (int x = 0; x < board.getCols(); x++)
                            if (board.getMino(y, x).isFilled())
                                break find_y;

                    for (int x = 0; x < board.getCols(); x++)
                        board.getMino(y+1, x).set(board, board.getMino(y, x));
                }
                this.isDone = true;
            }
        });
    }


    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(UPGRADED_COST);
            initializeDescription();
        }
    }
}

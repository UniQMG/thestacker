package theStacker.cards.skill.gravity;

import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.GravityPower;
import theStacker.util.Helpers;

import static theStacker.TheStackerMod.makeCardPath;

public class GreatWall extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(GreatWall.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public GreatWall() {
        super(ID, IMG, 2, CardType.SKILL, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.UNCOMMON, CardTarget.NONE);
        this.baseMagicNumber = 5;
        this.magicNumber = 5;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        if (!(player instanceof MatrixHolder)) return;

        int max = this.magicNumber * Helpers.powerStacks(player, GravityPower.POWER_ID);
        int minos = Helpers.minosFilled(((MatrixHolder) player).getMatrix().board) / 5;

        addToBot(new GainBlockAction(player, Math.min(max, minos)));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(3);
            initializeDescription();
        }
    }
}

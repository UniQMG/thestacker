package theStacker.cards.skill.gravity;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.GravityPower;

import static theStacker.TheStackerMod.makeCardPath;

public class Singularity extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Singularity.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public Singularity() {
        super(ID, IMG, 2, CardType.SKILL, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.UNCOMMON, CardTarget.NONE);
        this.baseMagicNumber = 20;
        this.magicNumber = 20;
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new ApplyPowerAction(player, player, new GravityPower(player, this.magicNumber)));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(1);
            initializeDescription();
        }
    }
}

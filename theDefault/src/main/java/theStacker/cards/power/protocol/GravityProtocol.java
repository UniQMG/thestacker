package theStacker.cards.power.protocol;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageRandomEnemyAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import theStacker.powers.GravityPower;
import theStacker.util.Helpers;

import static theStacker.powers.cards.protocol.ProtocolProvider.Source.QUAD;
import static theStacker.powers.cards.protocol.ProtocolProvider.Source.TSPIN;

public class GravityProtocol extends ProtocolCard {
    public GravityProtocol() {
        super("Gravity", CardRarity.COMMON, 1);
    }

    @Override
    public int adjustNumber(AbstractCreature player, Source source, int index, int upgrade) {
        int gravity = Helpers.powerStacks(player, GravityPower.POWER_ID);
        switch (index) {
            case 0: return 2+upgrade;
            case 1: return gravity * (2+upgrade);
            default: return 0;
        }
    }

    @Override
    public void onPerfectClear(AbstractCreature creature, int upgrade) {
    }

    @Override
    public void onTSpin(AbstractCreature creature, int upgrade, int lines, boolean mini) {
        int damage = adjustNumber(creature, TSPIN, 0, upgrade);
        addToBot(new DamageRandomEnemyAction(
            new DamageInfo(creature, damage, DamageInfo.DamageType.NORMAL),
            AbstractGameAction.AttackEffect.BLUNT_LIGHT
        ));
    }

    @Override
    public void onQuad(AbstractCreature creature, int upgrade) {
        int damage = adjustNumber(creature, QUAD, 0, upgrade);
        addToBot(new DamageRandomEnemyAction(
            new DamageInfo(creature, damage, DamageInfo.DamageType.NORMAL),
            AbstractGameAction.AttackEffect.BLUNT_LIGHT
        ));
    }

    @Override
    public int minB2B() {
        return 0;
    }

    @Override
    public void onB2B(AbstractCreature creature, int upgrade, int b2b) {
    }

    @Override
    public int minCombo() {
        return 0;
    }

    @Override
    public void onCombo(AbstractCreature creature, int upgrade, int combo) {
    }
}

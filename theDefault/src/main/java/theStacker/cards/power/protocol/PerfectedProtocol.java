package theStacker.cards.power.protocol;

import basemod.AutoAdd;
import com.evacipated.cardcrawl.mod.stslib.fields.cards.AbstractCard.GraveField;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.ArtifactPower;
import com.megacrit.cardcrawl.powers.PlatedArmorPower;

import static theStacker.powers.cards.protocol.ProtocolProvider.Source.*;

@AutoAdd.Ignore // These are dumb and need to be reworked or removed
public class PerfectedProtocol extends ProtocolCard {
    public PerfectedProtocol() {
        super("Perfected", CardRarity.RARE, 3);
    }

    @Override
    public boolean canPlay(AbstractCard card) {
        if (!(card instanceof PerfectedProtocol)) return super.canPlay(card);
        boolean defense = false, offense = false, support = false;
        for (AbstractCard abstractCard : AbstractDungeon.actionManager.cardsPlayedThisCombat) {
            if (abstractCard instanceof PerfectDefenseProtocol) defense = true;
            if (abstractCard instanceof PerfectOffenseProtocol) offense = true;
            if (abstractCard instanceof PerfectSupportProtocol) support = true;
        }
        return super.canPlay(card) && defense && offense && support;
    }

    @Override
    public void upgrade() {
        if (!upgraded) GraveField.grave.set(this, true);
        super.upgrade();
    }

    @Override
    @SuppressWarnings("PointlessArithmeticExpression") // Because keeping things nicely aligned is "pointless"
    public int adjustNumber(AbstractCreature player, Source source, int index, int upgrade) {
        switch (index) {
            case 0: return 10 + 5 * upgrade; // HP healing
            case 1: return 20 + 5 * upgrade; // Block
            case 2: return 25 + 5 * upgrade; // Damage
            case 3: return  5 + 2 * upgrade; // Plated armor
            case 4: return  1 + 1 * upgrade; // Artifact
        }
        return 0;
    }

    @Override
    public void onPerfectClear(AbstractCreature creature, int upgrade) {
        creature.heal(adjustNumber(creature, PC, 0, upgrade));
        creature.addBlock(adjustNumber(creature, PC, 1, upgrade));
        if (creature instanceof AbstractPlayer) {
            int damage = adjustNumber(creature, PC, 2, upgrade);
            addToBot(new DamageAllEnemiesAction(
                (AbstractPlayer) creature,
                damage,
                DamageInfo.DamageType.NORMAL,
                AbstractGameAction.AttackEffect.BLUNT_LIGHT
            ));
        }
        addToBot(new ApplyPowerAction(
            creature,
            creature,
            new PlatedArmorPower(creature, adjustNumber(creature, PC, 3, upgrade))
        ));
        addToBot(new ApplyPowerAction(
            creature,
            creature,
            new ArtifactPower(creature, adjustNumber(creature, PC, 4, upgrade))
        ));
    }

    @Override
    public void onTSpin(AbstractCreature creature, int upgrade, int lines, boolean mini) {
    }

    @Override
    public void onQuad(AbstractCreature creature, int upgrade) {
    }

    @Override
    public int minB2B() {
        return 0;
    }

    @Override
    public void onB2B(AbstractCreature creature, int upgrade, int b2b) {
    }

    @Override
    public int minCombo() {
        return 0;
    }

    @Override
    public void onCombo(AbstractCreature creature, int upgrade, int combo) {
    }
}

package theStacker.cards.power.protocol;

import basemod.AutoAdd;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.cards.protocol.ProtocolPower;
import theStacker.powers.cards.protocol.ProtocolProvider;

import static theStacker.TheStackerMod.makeCardPath;
import static theStacker.TheStackerMod.makeID;

@AutoAdd.Ignore
public abstract class ProtocolCard extends AbstractDynamicCard implements ProtocolProvider  {
    protected final CardStrings cardStrings;

    private final String protocolId;

    protected ProtocolCard(String protocolId, CardRarity rarity, int cost) {
        super(
            makeID("Protocol" + protocolId),
            // Replace this based on protocol ID once we start actually making art
            makeCardPath("placeholder.png"),
            cost,
            CardType.POWER,
            TheStacker.Enums.STACKER_CYAN_COLOR,
            rarity,
            CardTarget.NONE
        );
        this.protocolId = protocolId;
        this.cardStrings = CardCrawlGame.languagePack.getCardStrings(this.cardID);
        // Used to increase values in the protocol card callbacks
        // so that they're upgradable
        this.baseMagicNumber = 0;
        this.magicNumber = 0;
        updateDescription();
    }

    private void updateDescription() {

        this.rawDescription = this.cardStrings.DESCRIPTION + ProtocolPower.getProtocolString(this, AbstractDungeon.player);
        initializeDescription();
    }

    @Override
    public String getProtocolName() {
        return protocolId;
    }

    @Override
    public int getUpgradeCount() {
        return magicNumber;
    }

    @Override
    public String perfectClearString() {
        return cardStrings.EXTENDED_DESCRIPTION[0];
    }

    @Override
    public String tSpinString() {
        return cardStrings.EXTENDED_DESCRIPTION[1];
    }

    @Override
    public String quadString() {
        return cardStrings.EXTENDED_DESCRIPTION[2];
    }

    @Override
    public String b2bString() {
        return cardStrings.EXTENDED_DESCRIPTION[3];
    }

    @Override
    public String comboString() {
        return cardStrings.EXTENDED_DESCRIPTION[4];
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        if (!(player instanceof MatrixHolder)) return;
        if (player.hasPower(ProtocolPower.POWER_ID))
            addToBot(new RemoveSpecificPowerAction(player, player, ProtocolPower.POWER_ID));
        addToBot(new ApplyPowerAction(
            player,
            player,
            new ProtocolPower(this, (MatrixHolder) player, player, this)
        ));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(1);
            updateDescription();
        }
    }
}

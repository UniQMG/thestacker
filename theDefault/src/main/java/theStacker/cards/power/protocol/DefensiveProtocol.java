package theStacker.cards.power.protocol;
import com.megacrit.cardcrawl.core.AbstractCreature;

import static theStacker.powers.cards.protocol.ProtocolProvider.Source.*;

public class DefensiveProtocol extends ProtocolCard {
    public DefensiveProtocol() {
        super("Defensive", CardRarity.UNCOMMON, 1);
    }

    @Override
    public int adjustNumber(AbstractCreature player, Source source, int index, int upgrade) {
        return 12 + 3 * upgrade;
    }

    @Override
    public void onPerfectClear(AbstractCreature creature, int upgrade) {
        creature.addBlock(adjustNumber(creature, PC, 0, upgrade));
    }

    @Override
    public void onTSpin(AbstractCreature creature, int upgrade, int lines, boolean mini) {
        creature.addBlock(adjustNumber(creature, TSPIN, 0, upgrade));
    }

    @Override
    public void onQuad(AbstractCreature creature, int upgrade) {
        creature.addBlock(adjustNumber(creature, QUAD, 0, upgrade));
    }

    @Override
    public int minB2B() {
        return 0;
    }

    @Override
    public void onB2B(AbstractCreature creature, int upgrade, int b2b) {
    }

    @Override
    public int minCombo() {
        return 0;
    }

    @Override
    public void onCombo(AbstractCreature creature, int upgrade, int combo) {
    }
}

package theStacker.cards.power;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.cards.FreezerEnergyLossPower;
import theStacker.powers.cards.FreezerPower;

import static theStacker.TheStackerMod.makeCardPath;

public class Freezer extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Freezer.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public Freezer() {
        super(ID, IMG, 3, CardType.POWER, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.RARE, CardTarget.NONE);
        this.baseMagicNumber = 1;
        this.magicNumber = 1;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new ApplyPowerAction(player, player, new FreezerEnergyLossPower(player, player, 1)));
        addToBot(new ApplyPowerAction(player, player, new FreezerPower(player, player, magicNumber)));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(1);
            initializeDescription();
        }
    }
}

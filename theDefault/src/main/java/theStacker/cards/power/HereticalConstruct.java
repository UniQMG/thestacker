package theStacker.cards.power;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.cards.HereticalConstructPower;

import static theStacker.TheStackerMod.makeCardPath;

public class HereticalConstruct extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(HereticalConstruct.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    private static final CardRarity RARITY = CardRarity.UNCOMMON;
    private static final CardTarget TARGET = CardTarget.NONE;
    private static final CardType TYPE = CardType.POWER;
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);
    public static final CardColor COLOR = TheStacker.Enums.STACKER_CYAN_COLOR;
    private static final int COST = 3;

    public HereticalConstruct() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        this.baseMagicNumber = 7;
        this.magicNumber = 7;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        if (!(player instanceof MatrixHolder)) return;
        AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(
            player,
            player,
            new HereticalConstructPower((MatrixHolder) player, player, player, 1)
        ));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(7);
            initializeDescription();
        }
    }
}

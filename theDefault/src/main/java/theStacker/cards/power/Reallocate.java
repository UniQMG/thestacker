package theStacker.cards.power;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DrawReductionPower;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.cards.ReallocatePower;

import static theStacker.TheStackerMod.makeCardPath;

public class Reallocate extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Reallocate.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    private static final CardRarity RARITY = CardRarity.COMMON;
    private static final CardTarget TARGET = CardTarget.NONE;
    private static final CardType TYPE = CardType.POWER;
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);
    public static final CardColor COLOR = TheStacker.Enums.STACKER_CYAN_COLOR;

    private static final int COST = 1;
    private static final int UPGRADED_COST = 1;

    private static final int PIECES_PER_CARD = 2;
    private static final int PIECES_PER_CARD_UPGRADE_BONUS = 1;

    public Reallocate() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        this.baseMagicNumber = PIECES_PER_CARD;
        this.magicNumber = PIECES_PER_CARD;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        if (!(player instanceof MatrixHolder)) return;
        AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(
            player,
            player,
            new DrawReductionPower(player, 1)
        ));
        AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(
            player,
            player,
            new ReallocatePower((MatrixHolder) player, player, player, magicNumber)
        ));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(UPGRADED_COST);
            // rawDescription = cardStrings.UPGRADE_DESCRIPTION;
            upgradeMagicNumber(PIECES_PER_CARD_UPGRADE_BONUS);
            initializeDescription();
        }
    }
}

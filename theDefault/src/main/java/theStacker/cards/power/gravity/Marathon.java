package theStacker.cards.power.gravity;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.cards.MarathonPower;

import static theStacker.TheStackerMod.makeCardPath;

public class Marathon extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Marathon.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);

    public Marathon() {
        super(ID, IMG, 1, CardType.POWER, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.UNCOMMON, CardTarget.NONE);
        this.baseMagicNumber = 1;
        this.magicNumber = baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {

        addToBot(new ApplyPowerAction(player, player, new MarathonPower(player, player, 1)));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(1);
            initializeDescription();
        }
    }
}

package theStacker.cards.power;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.cards.RecyclePower;

import static theStacker.TheStackerMod.makeCardPath;

public class Recycle extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Recycle.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    private static final CardRarity RARITY = CardRarity.UNCOMMON;
    private static final CardTarget TARGET = CardTarget.NONE;
    private static final CardType TYPE = CardType.POWER;
    private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings(ID);
    public static final CardColor COLOR = TheStacker.Enums.STACKER_CYAN_COLOR;

    private static final int COST = 1;
    private static final int UPGRADED_COST = 0;

    public Recycle() {
        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        if (!(player instanceof MatrixHolder)) return;

        AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(
            player,
            player,
            new RecyclePower((MatrixHolder) player, player, player, 1)
        ));
    }

    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeBaseCost(UPGRADED_COST);
            initializeDescription();
        }
    }
}

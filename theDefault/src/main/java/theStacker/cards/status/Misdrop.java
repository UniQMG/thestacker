package theStacker.cards.status;

import com.megacrit.cardcrawl.actions.common.ExhaustSpecificCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.stacking.Controls;
import theStacker.stacking.Matrix;

import static theStacker.TheStackerMod.makeCardPath;

/**
 * A horrifying status that has a small chance to exhaust and shift your piece to the side when hard dropping
 */
public class Misdrop extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Misdrop.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    private static final Object key = new Object();
    static {
        Matrix.create.listen(key, matrix -> {
            if (!(matrix.owner instanceof AbstractPlayer))
                return true;

            matrix.input.get(Controls.GameKey.HARD_DROP).listen(key, () -> {
                CardGroup hand = ((AbstractPlayer) matrix.owner).hand;
                AbstractCard card = hand.findCardById(ID);
                if (card == null || matrix.piece == null || Math.random() < 0.75)
                    return true;

                boolean left = matrix.piece.canMove(matrix.board, -1, 0);
                boolean right = matrix.piece.canMove(matrix.board, 1, 0);
                if (!left || !right)
                    return true;

                AbstractDungeon.actionManager.addToBottom(new ExhaustSpecificCardAction(card, hand));
                matrix.piece.move(Math.random() > 0.5 ? 1 : -1, 0);
                return true;
            });

            return true;
        });
    }

    public Misdrop() {
        super(ID, IMG, -2, CardType.STATUS, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.SPECIAL, CardTarget.NONE);
        this.retain = true;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
    }

    @Override
    public void upgrade() {
    }
}

package theStacker.cards.status;

import basemod.AutoAdd;
import com.evacipated.cardcrawl.mod.stslib.actions.common.AutoplayCardAction;
import com.evacipated.cardcrawl.mod.stslib.fields.cards.AbstractCard.AutoplayField;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.actions.QueuePieceAction;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;
import theStacker.powers.cards.StackerFormPower;
import theStacker.stacking.Piece;
import theStacker.util.Helpers;

import static theStacker.TheStackerMod.makeCardPath;
import static theStacker.TheStackerMod.makeID;

@AutoAdd.Ignore
public class PieceCard extends AbstractDynamicCard {
    public static final String IMG = makeCardPath("placeholder.png");

    private final Piece.PieceType type;

    public PieceCard(Piece.PieceType type) {
        super(
            makeID("PieceCard" + type),
            IMG,
            0,
            CardType.STATUS,
            TheStacker.Enums.STACKER_CYAN_COLOR,
            CardRarity.SPECIAL,
            CardTarget.NONE
        );
        this.exhaust = true;
        this.isEthereal = true;
        this.type = type;
    }

    @Override
    public void triggerWhenDrawn() {
        int stacks = Helpers.powerStacks(AbstractDungeon.player, StackerFormPower.POWER_ID);
        if (stacks == 0)
            addToBot(new AutoplayCardAction(this, AbstractDungeon.player.hand));
    }

    @Override
    public void onMoveToDiscard() {
        AbstractDungeon.player.discardPile.moveToExhaustPile(this);
    }

    @Override
    public void triggerOnEndOfTurnForPlayingCard() {
//        if (AbstractDungeon.player instanceof TheStacker) {
//            addToBot(new IncreasePieceExhaustDamageAction());
//            addToBot(new DamageAction(
//                AbstractDungeon.player,
//                new DamageInfo(
//                    AbstractDungeon.player,
//                    Formulas.pieceExhaustDamage(((TheStacker) AbstractDungeon.player).pieceCardsExhausted),
//                    DamageInfo.DamageType.HP_LOSS
//                )
//            ));
//        }

//        this.initializeDescription();
    }

    @Override
    public AbstractCard makeCopy() {
        return new PieceCard(type);
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        addToBot(new DrawCardAction(1));
        addToBot(new QueuePieceAction(this.type));
    }

    @Override
    public boolean canUpgrade() {
        return false;
    }

    @Override
    public void upgrade() {
    }

    public Piece.PieceType getPiece() {
        return type;
    }
}

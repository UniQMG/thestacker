package theStacker.cards.status;

import com.megacrit.cardcrawl.cards.CardQueueItem;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.MatrixHolder;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;

import static theStacker.TheStackerMod.makeCardPath;
import static theStacker.TheStackerMod.makeID;

/**
 * Status card that adds cheese to your board
 */
public class Cheesed extends AbstractDynamicCard {
    public static final String ID = makeID(Cheesed.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    public Cheesed() {
        super(ID, IMG, -2, CardType.STATUS, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.SPECIAL, CardTarget.NONE);
        this.isEthereal = true;
    }

    @Override
    public void triggerOnEndOfTurnForPlayingCard() {
        AbstractDungeon.actionManager.cardQueue.add(new CardQueueItem(this, true));
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
        if (AbstractDungeon.player instanceof MatrixHolder) {
            ((MatrixHolder) AbstractDungeon.player).getMatrix().addGarbage(2, 1, false);
        }
    }

    @Override
    public void upgrade() {
    }
}

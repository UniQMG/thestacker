package theStacker.cards.status;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import theStacker.TheStackerMod;
import theStacker.cards.AbstractDynamicCard;
import theStacker.characters.TheStacker;

import static theStacker.TheStackerMod.makeCardPath;

/**
 * Status card added to your hand whenever you reach an 18 combo
 * Does nothing and exhausts at end of turn
 */
public class Fourwide extends AbstractDynamicCard {
    public static final String ID = TheStackerMod.makeID(Fourwide.class.getSimpleName());
    public static final String IMG = makeCardPath("placeholder.png");

    public Fourwide() {
        super(ID, IMG, -2, CardType.STATUS, TheStacker.Enums.STACKER_CYAN_COLOR, CardRarity.SPECIAL, CardTarget.NONE);
        this.isEthereal = true;
    }

    @Override
    public void use(AbstractPlayer player, AbstractMonster monster) {
    }

    @Override
    public void upgrade() {
    }
}

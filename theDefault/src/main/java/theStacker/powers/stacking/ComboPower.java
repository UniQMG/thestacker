package theStacker.powers.stacking;

import basemod.interfaces.CloneablePowerInterface;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import theStacker.TheStackerMod;
import theStacker.MatrixHolder;
import theStacker.util.Formulas;
import theStacker.util.TextureLoader;

import static theStacker.TheStackerMod.makePowerPath;

// This is just a display-only power, actual combo calcs are handled in the Matrix
public class ComboPower extends AbstractPower implements CloneablePowerInterface {
    public AbstractCreature source;

    public static final String POWER_ID = TheStackerMod.makeID("ComboPower");
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("placeholder_power84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("placeholder_power32.png"));

    public ComboPower(AbstractCreature owner, int amount) {
        name = NAME;
        ID = POWER_ID;

        this.owner = owner;
        this.amount = amount;
        this.source = owner;

        type = PowerType.BUFF;
        isTurnBased = false;

        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);

        this.loadRegion("buffer");
        updateDescription();
    }

    @Override
    public void atEndOfTurn(boolean isPlayer) {
        if (owner instanceof MatrixHolder)
            ((MatrixHolder) owner).getMatrix().onTurnEnd(); // todo: move this somewhere else
    }

    @Override
    public void updateDescription() {
        this.description = DESCRIPTIONS[0] + Formulas.comboDamage(0, 0) + DESCRIPTIONS[1];
    }

    @Override
    public AbstractPower makeCopy() {
        return new ComboPower(owner, amount);
    }
}

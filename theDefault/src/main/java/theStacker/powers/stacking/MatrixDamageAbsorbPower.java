package theStacker.powers.stacking;

import basemod.interfaces.CloneablePowerInterface;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import theStacker.TheStackerMod;
import theStacker.util.TextureLoader;

import static theStacker.TheStackerMod.makePowerPath;

public class MatrixDamageAbsorbPower extends AbstractPower implements CloneablePowerInterface {
    public static final String POWER_ID = TheStackerMod.makeID(MatrixDamageAbsorbPower.class.getSimpleName());
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("cards/MatrixDamageAbsorb/84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("cards/MatrixDamageAbsorb/32.png"));

    public MatrixDamageAbsorbPower(AbstractCreature owner, int amount) {
        this.name = NAME;
        this.ID = POWER_ID;
        this.owner = owner;
        this.amount = amount;

        this.type = PowerType.BUFF;
        this.canGoNegative = false;
        this.isTurnBased = false;

        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);

        this.description = DESCRIPTIONS[0];
    }

    public int onAttackedToChangeDamage(DamageInfo info, int damageAmount) {
        if (info.type != DamageInfo.DamageType.HP_LOSS) {
            addToBot(new ApplyPowerAction(owner, owner, new IncomingGarbagePower(owner, damageAmount)));
            return 0;
        }
        return damageAmount;
    }

    @Override
    public AbstractPower makeCopy() {
        return new MatrixDamageAbsorbPower(owner, amount);
    }
}

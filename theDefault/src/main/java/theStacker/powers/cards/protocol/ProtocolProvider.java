package theStacker.powers.cards.protocol;

import com.megacrit.cardcrawl.core.AbstractCreature;

public interface ProtocolProvider {
    enum Source { PC, TSPIN, QUAD, B2B, COMBO }

    /**
     * Sets a number placeholder based on the upgrade count/magic number of the card
     *
     * @param player
     * @param source what type of number is being replaced
     * @param index what position the number is
     * @param upgrade the magic number/upgrade count to upgrade the number by
     * @return the placeholder value
     */
    int adjustNumber(AbstractCreature player, Source source, int index, int upgrade);

    int getUpgradeCount();

    /**
     * @return the name of the protocol
     */
    String getProtocolName();

    /**
     * The string to display for the perfect clear callback
     * @return the string, or null
     */
    String perfectClearString();
    void onPerfectClear(AbstractCreature creature, int upgrade);

    /**
     * The string to display for the T spin callback
     * @return the string, or null
     */
    String tSpinString();
    void onTSpin(AbstractCreature creature, int upgrade, int lines, boolean mini);


    /**
     * The string to display for the 4 line callback
     * @return the string, or null
     */
    String quadString();
    void onQuad(AbstractCreature creature, int upgrade);

    /**
     * The string to display for the back-to-back amount
     * @return the string, or null
     */
    String b2bString();
    int minB2B();
    void onB2B(AbstractCreature creature, int upgrade, int b2b);


    /**
     * The string to display for the combo amount
     * @return the string, or null
     */
    String comboString();
    int minCombo();
    void onCombo(AbstractCreature creature, int upgrade, int combo);
}

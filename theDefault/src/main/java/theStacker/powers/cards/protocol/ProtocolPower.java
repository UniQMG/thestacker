package theStacker.powers.cards.protocol;

import basemod.interfaces.CloneablePowerInterface;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import theStacker.MatrixHolder;
import theStacker.cards.power.protocol.ProtocolCard;
import theStacker.stacking.LineClearInfo;
import theStacker.util.TextureLoader;

import theStacker.TheStackerMod;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static theStacker.TheStackerMod.makePowerPath;

public class ProtocolPower extends AbstractPower implements CloneablePowerInterface {
    public AbstractCreature source;

    public static final String POWER_ID = TheStackerMod.makeID("ProtocolPower");
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("placeholder_power84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("placeholder_power32.png"));
    private ProtocolCard returnCard;
    private MatrixHolder holder;
    private ProtocolProvider provider;
    private boolean removed = false;

    public ProtocolPower(ProtocolCard returnCard, MatrixHolder holder, AbstractCreature owner, ProtocolProvider provider) {
        this.returnCard = returnCard;
        this.holder = holder;
        this.provider = provider;
        this.owner = owner;
        this.source = owner;
        this.amount = 1;

        name = NAME + provider.getProtocolName();
        ID = POWER_ID;

        type = PowerType.BUFF;
        isTurnBased = false;
        canGoNegative = false;

        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);

        this.loadRegion("buffer");
        updateDescription();

        holder.getMatrix().pieceLock.listen(this, this::onPieceLock);
    }

    @Override
    public void onRemove() {
        if (owner instanceof AbstractPlayer) {
            addToBot(new MakeTempCardInHandAction(returnCard));
        }
        removed = true;
    }

    private boolean onPieceLock(LineClearInfo info) {
        if (info.tspin)
            provider.onTSpin(owner, provider.getUpgradeCount(), info.lines, info.mini);

        if (info.perfectclear)
            provider.onPerfectClear(owner, provider.getUpgradeCount());

        if (info.b2b > 0)
            provider.onB2B(owner, provider.getUpgradeCount(), info.b2b);

        if (info.combo > 0)
            provider.onCombo(owner, provider.getUpgradeCount(), info.combo);

        if (info.lines >= 4)
            provider.onQuad(owner, provider.getUpgradeCount());

        return !removed;
    }

    private static final int PC = 2;
    private static final int T_SPIN = 3;
    private static final int QUAD = 4;
    private static final int B2B = 5;
    private static final int COMBO = 6;
    private static final int PUNCTUATION = 7;

    // Update the description when you apply this power. (i.e. add or remove an "s" in keyword(s))
    @Override
    public void updateDescription() {
        this.description = (
            DESCRIPTIONS[0] +
            provider.getProtocolName() +
            DESCRIPTIONS[1] +
            getProtocolString(provider, owner)
        );
    }

    private static Pattern pattern = Pattern.compile("\\{}", 0);
    private static String performReplace(ProtocolProvider provider, AbstractCreature creature, ProtocolProvider.Source source, String string) {
        StringBuffer replaced = new StringBuffer();

        int index = 0;
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            int val = provider.adjustNumber(creature, source, index, provider.getUpgradeCount());
            matcher.appendReplacement(replaced, String.valueOf(val));
            index++;
        }
        matcher.appendTail(replaced);

        return replaced.toString();
    }

    public static String getProtocolString(ProtocolProvider provider, AbstractCreature creature) {
        StringBuilder builder = new StringBuilder(64);

        if (provider.perfectClearString().length() > 0)
            builder
                .append(DESCRIPTIONS[PC])
                .append(DESCRIPTIONS[PUNCTUATION])
                .append(performReplace(provider, creature, ProtocolProvider.Source.PC, provider.perfectClearString()));

        if (provider.tSpinString().length() > 0)
            builder
                .append(DESCRIPTIONS[T_SPIN])
                .append(DESCRIPTIONS[PUNCTUATION])
                .append(performReplace(provider, creature, ProtocolProvider.Source.TSPIN, provider.tSpinString()));

        if (provider.quadString().length() > 0)
            builder
                .append(DESCRIPTIONS[QUAD])
                .append(DESCRIPTIONS[PUNCTUATION])
                .append(performReplace(provider, creature, ProtocolProvider.Source.QUAD, provider.quadString()));

        if (provider.b2bString().length() > 0)
            builder
                .append(DESCRIPTIONS[B2B])
                .append(" ")
                .append(provider.minB2B())
                .append(DESCRIPTIONS[PUNCTUATION])
                .append(performReplace(provider, creature, ProtocolProvider.Source.B2B, provider.b2bString()));

        if (provider.comboString().length() > 0)
            builder
                .append(DESCRIPTIONS[COMBO])
                .append(" ")
                .append(provider.minCombo())
                .append(DESCRIPTIONS[PUNCTUATION])
                .append(performReplace(provider, creature, ProtocolProvider.Source.COMBO, provider.comboString()));

        return builder.toString();
    }

    @Override
    public AbstractPower makeCopy() {
        return new ProtocolPower(returnCard, holder, owner, provider);
    }
}

package theStacker.powers.cards;

import basemod.interfaces.CloneablePowerInterface;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.stacking.Matrix;
import theStacker.stacking.Piece;
import theStacker.util.TextureLoader;

import static theStacker.TheStackerMod.makePowerPath;

//Gain 1 dex for the turn for each card played.

public class HereticalConstructPower extends AbstractPower implements CloneablePowerInterface {
    public AbstractCreature source;

    public static final String POWER_ID = TheStackerMod.makeID(HereticalConstructPower.class.getSimpleName());
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("cards/HereticalConstruct/84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("cards/HereticalConstruct/32.png"));

    private MatrixHolder stacker;
    private int piecesProvided = 0;
    private boolean cancelled = false;
    public HereticalConstructPower(MatrixHolder stacker, AbstractCreature owner, AbstractCreature source, final int amount) {
        name = NAME;
        ID = POWER_ID;

        this.stacker = stacker;
        this.owner = owner;
        this.amount = amount;
        this.source = source;

        type = PowerType.DEBUFF;
        isTurnBased = false;
        canGoNegative = false;

        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);

        updateDescription();

        stacker.getMatrix().pieceLock.listen(this, this::providePieces);
        this.providePieces();
    }

    private boolean providePieces() {
        piecesProvided++;

        Matrix matrix = stacker.getMatrix();
        while (matrix.queue.size() < 5) {
            for (int i = 0; i < this.amount; i++) {
                matrix.queuePiece(Piece.get(matrix.getNextPieceFromGenerator()));
                addToBot(new DamageAction(
                    owner,
                    new DamageInfo(
                        owner,
                        piecesProvided / 7 + 1,
                        DamageInfo.DamageType.HP_LOSS
                    ),
                    AbstractGameAction.AttackEffect.BLUNT_LIGHT
                ));
            }
        }

        updateDescription();

        return !this.cancelled && this.amount > 0;
    }

    @Override
    public void onRemove() {
        this.cancelled = true;
    }

    @Override
    public boolean canPlayCard(AbstractCard card) {
        return false;
    }

    @Override
    public void updateDescription() {
        description = DESCRIPTIONS[0] + (piecesProvided / 7 + 1) + DESCRIPTIONS[1] + (amount * 7) + DESCRIPTIONS[2];
    }

    @Override
    public AbstractPower makeCopy() {
        HereticalConstructPower power = new HereticalConstructPower(stacker, owner, source, amount);
        power.piecesProvided = this.piecesProvided;
        power.cancelled = this.cancelled;
        return power;
    }
}

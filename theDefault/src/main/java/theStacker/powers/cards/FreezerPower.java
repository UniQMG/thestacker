package theStacker.powers.cards;

import basemod.interfaces.CloneablePowerInterface;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import theStacker.MatrixHolder;
import theStacker.stacking.Kick;
import theStacker.stacking.Piece;
import theStacker.util.TextureLoader;

import theStacker.TheStackerMod;

import static theStacker.TheStackerMod.makePowerPath;

public class FreezerPower extends AbstractPower implements CloneablePowerInterface {
    public AbstractCreature source;

    public static final String POWER_ID = TheStackerMod.makeID(FreezerPower.class.getSimpleName());
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("cards/Freezer/84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("cards/Freezer/32.png"));

    public FreezerPower(AbstractCreature owner, AbstractCreature source, final int amount) {
        name = NAME;
        ID = POWER_ID;

        this.owner = owner;
        this.amount = amount;
        this.source = source;

        type = PowerType.BUFF;
        isTurnBased = false;
        canGoNegative = false;

        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);

        updateDescription();
    }

    @Override
    public void atStartOfTurn() {
        if (!(owner instanceof MatrixHolder)) return;
        for (int i = 0; i < amount; i++) {
            ((MatrixHolder) owner).getMatrix().queuePiece(new Piece(
                null,
                1,
                Color.YELLOW,
                new int[][]{{0,0}},
                new Kick[0],
                (board, mino) -> {
                    if (board != ((MatrixHolder) owner).getMatrix().board) return;
                    addToBot(new GainEnergyAction(1));
                }
            ));
        }
    }

    @Override
    public void onApplyPower(AbstractPower power, AbstractCreature target, AbstractCreature source) {
        super.onApplyPower(power, target, source);
    }

    private boolean cancelled = false;

    @Override
    public void onRemove() {
        this.cancelled = true;
    }

    // Update the description when you apply this power. (i.e. add or remove an "s" in keyword(s))
    @Override
    public void updateDescription() {
        description = DESCRIPTIONS[0] + amount + DESCRIPTIONS[1];
    }

    @Override
    public AbstractPower makeCopy() {
        return new FreezerPower(owner, source, amount);
    }
}

package theStacker.powers.cards;

import basemod.interfaces.CloneablePowerInterface;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import theStacker.MatrixHolder;
import theStacker.cards.status.PieceCard;
import theStacker.util.Helpers;
import theStacker.util.TextureLoader;

import theStacker.TheStackerMod;

import static theStacker.TheStackerMod.makePowerPath;

public class ReallocatePower extends AbstractPower implements CloneablePowerInterface {
    public AbstractCreature source;

    public static final String POWER_ID = TheStackerMod.makeID("ReallocatePower");
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("cards/Reallocate/84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("cards/Reallocate/32.png"));
    private MatrixHolder holder;

    public ReallocatePower(MatrixHolder holder, AbstractCreature owner, AbstractCreature source, final int amount) {
        this.holder = holder;
        name = NAME;
        ID = POWER_ID;

        this.owner = owner;
        this.amount = amount;
        this.source = source;

        type = PowerType.BUFF;
        isTurnBased = false;
        canGoNegative = false;

        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);

        this.loadRegion("buffer");
        updateDescription();
    }

    @Override
    public void atStartOfTurnPostDraw() {
        if (owner instanceof AbstractPlayer) {
            for (int i = 0; i < this.amount; i++) {
                PieceCard next = new PieceCard(holder.getMatrix().generator.next());
                Helpers.queuePieceCard(next);
            }
        }
    }

    // Update the description when you apply this power. (i.e. add or remove an "s" in keyword(s))
    @Override
    public void updateDescription() {
        description = DESCRIPTIONS[0] + amount + DESCRIPTIONS[1];
    }

    @Override
    public AbstractPower makeCopy() {
        return new ReallocatePower(holder, owner, source, amount);
    }
}

package theStacker.powers.cards;

import basemod.interfaces.CloneablePowerInterface;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import theStacker.MatrixHolder;
import theStacker.TheStackerMod;
import theStacker.cards.status.PieceCard;
import theStacker.stacking.Matrix;
import theStacker.util.Helpers;
import theStacker.util.TextureLoader;

import static theStacker.TheStackerMod.makePowerPath;

public class RecyclePower extends AbstractPower implements CloneablePowerInterface {
    public AbstractCreature source;

    public static final String POWER_ID = TheStackerMod.makeID("RecyclePower");
    private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings(POWER_ID);
    public static final String NAME = powerStrings.NAME;
    public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
    private static final Texture tex84 = TextureLoader.getTexture(makePowerPath("cards/Recycle/84.png"));
    private static final Texture tex32 = TextureLoader.getTexture(makePowerPath("cards/Recycle/32.png"));
    private MatrixHolder holder;
    private boolean removed = false;

    public RecyclePower(MatrixHolder holder, AbstractCreature owner, AbstractCreature source, final int amount) {
        name = NAME;
        ID = POWER_ID;

        this.holder = holder;
        this.owner = owner;
        this.amount = amount;
        this.source = source;

        type = PowerType.BUFF;
        isTurnBased = false;
        canGoNegative = false;

        this.region128 = new TextureAtlas.AtlasRegion(tex84, 0, 0, 84, 84);
        this.region48 = new TextureAtlas.AtlasRegion(tex32, 0, 0, 32, 32);

        Matrix matrix = this.holder.getMatrix();
        matrix.pieceLock.listen(this, lineClearInfo -> {
            if (!(this.owner instanceof AbstractPlayer)) return !removed;

            if (lineClearInfo.lines >= 2) {
                for (int i = 0; i < this.amount; i++) {
                    PieceCard card = new PieceCard(this.holder.getMatrix().generator.next());
                    Helpers.queuePieceCard(card);
                }
                this.flash();
            }

            return !removed;
        });

        this.loadRegion("buffer");
        updateDescription();
    }

    @Override
    public void onRemove() {
        this.removed = true;
    }

    @Override
    public void updateDescription() {
        description = DESCRIPTIONS[0] + amount + DESCRIPTIONS[1];
    }

    @Override
    public AbstractPower makeCopy() {
        return new RecyclePower(holder, owner, source, amount);
    }
}

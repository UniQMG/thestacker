package theStacker.variables;

import basemod.abstracts.DynamicVariable;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import theStacker.MatrixHolder;
import theStacker.cards.skill.gravity.GreatWall;
import theStacker.powers.GravityPower;
import theStacker.util.Helpers;

import static theStacker.TheStackerMod.makeID;

public class GreatWallBlockVariable extends DynamicVariable {
    @Override
    public String key() {
        return makeID("GreatWallBlock");
    }

    @Override
    public boolean isModified(AbstractCard card) {
        return false;
    }

    @Override
    public int value(AbstractCard card) {
        return baseValue(card);
    }

    @Override
    public int baseValue(AbstractCard card) {
        if (!(card instanceof GreatWall)) return 0;
        if (!(AbstractDungeon.player instanceof MatrixHolder)) return 0;
        if (((MatrixHolder) AbstractDungeon.player).getMatrix() == null) return 0;
        int max = card.magicNumber * Helpers.powerStacks(AbstractDungeon.player, GravityPower.POWER_ID);
        int minos = Helpers.minosFilled(((MatrixHolder) AbstractDungeon.player).getMatrix().board) / 5;
        return Math.min(minos, max);
    }

    @Override
    public boolean upgraded(AbstractCard card) {
        return card.upgraded;
    }
}

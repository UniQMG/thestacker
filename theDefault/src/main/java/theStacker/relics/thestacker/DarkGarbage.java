package theStacker.relics.thestacker;

import basemod.abstracts.CustomRelic;
import com.badlogic.gdx.graphics.Texture;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import theStacker.TheStackerMod;
import theStacker.characters.TheStacker;
import theStacker.util.TextureLoader;

import static theStacker.TheStackerMod.makeRelicOutlinePath;
import static theStacker.TheStackerMod.makeRelicPath;

/**
 * TODO: This is bad, needs rework
 */
public class DarkGarbage extends CustomRelic {
    public static final String ID = TheStackerMod.makeID(DarkGarbage.class.getSimpleName() + "Relic");
    private static final Texture IMG = TextureLoader.getTexture(makeRelicPath("placeholder_relic.png"));
    private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic.png"));

    public DarkGarbage() {
        super(ID, IMG, OUTLINE, RelicTier.BOSS, LandingSound.SOLID);

    }

    public void onEquip() {
        ++AbstractDungeon.player.energy.energyMaster;
    }

    public void onUnequip() {
        --AbstractDungeon.player.energy.energyMaster;
    }

    boolean running = false;

    @Override
    public void atBattleStartPreDraw() {
        running = true;
        if (AbstractDungeon.player instanceof TheStacker) {
            TheStacker stacker = (TheStacker) AbstractDungeon.player;
            final long[] start = {System.currentTimeMillis()};
            stacker.getMatrix().nextTick.listen(this, () -> {
                while (System.currentTimeMillis() > start[0]) {
                    start[0] += 5000;
                    stacker.getMatrix().addGarbage(1, 0, true);
                    flash();
                }
                return running;
            });
        }
    }


    @Override
    public void onVictory() {
        running = false;
    }

    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }
}
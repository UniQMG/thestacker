package theStacker.relics.thestacker;

import basemod.abstracts.CustomRelic;
import com.badlogic.gdx.graphics.Texture;
import com.evacipated.cardcrawl.mod.stslib.relics.ClickableRelic;
import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardQueueItem;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.relics.AbstractRelic;
import theStacker.TheStackerMod;
import theStacker.cards.status.PieceCard;
import theStacker.characters.TheStacker;
import theStacker.stacking.Matrix;
import theStacker.stacking.Piece;
import theStacker.util.Helpers;
import theStacker.util.TextureLoader;

import static theStacker.TheStackerMod.makeRelicOutlinePath;
import static theStacker.TheStackerMod.makeRelicPath;

public class NextQueue extends CustomRelic implements ClickableRelic {
    public static final String ID = TheStackerMod.makeID(NextQueue.class.getSimpleName() + "Relic");
    private static final Texture IMG = TextureLoader.getTexture(makeRelicPath("placeholder_relic.png"));
    private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic.png"));

    public NextQueue() {
        super(ID, IMG, OUTLINE, AbstractRelic.RelicTier.STARTER, AbstractRelic.LandingSound.MAGICAL);
    }

    @Override
    public void atTurnStart() {
        if (firstTurn) {
            firstTurn = false;
            return;
        }
        flash();
        if (AbstractDungeon.player instanceof TheStacker) {
            Piece.PieceType piece = ((TheStacker) AbstractDungeon.player).getMatrix().getNextPieceFromGenerator();
            Helpers.queuePieceCard(new PieceCard(piece));
        }
    }


    boolean firstTurn = false;
    @Override
    public void atPreBattle() {
        firstTurn = true;
    }


    @Override
    public void onRightClick() {
        addToBot(new RelicAboveCreatureAction(AbstractDungeon.player, this));
        for (AbstractCard card : AbstractDungeon.player.hand.group) {
            if (card instanceof PieceCard) {
                AbstractDungeon.actionManager.addCardQueueItem(
                    new CardQueueItem(
                        card,
                        false,
                        card.energyOnUse,
                        true,
                        true
                    ),
                    false
                );
            }
        }
    }

    // Description
    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0] + " " + DESCRIPTIONS[1] + " " + DESCRIPTIONS[2];
    }
}
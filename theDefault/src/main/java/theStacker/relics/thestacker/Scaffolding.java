package theStacker.relics.thestacker;

import basemod.abstracts.CustomRelic;
import com.badlogic.gdx.graphics.Texture;
import theStacker.TheStackerMod;
import theStacker.util.TextureLoader;

import static theStacker.TheStackerMod.makeRelicOutlinePath;
import static theStacker.TheStackerMod.makeRelicPath;

/**
 * Reduces the piece fall speed effect from gravity by 75%
 * Logic handled in Matrix
 */
public class Scaffolding extends CustomRelic {
    public static final String ID = TheStackerMod.makeID(Scaffolding.class.getSimpleName() + "Relic");
    private static final Texture IMG = TextureLoader.getTexture(makeRelicPath("placeholder_relic.png"));
    private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic.png"));

    public Scaffolding() {
        super(ID, IMG, OUTLINE, RelicTier.COMMON, LandingSound.MAGICAL);
    }

    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }
}
package theStacker.relics.thestacker;

import basemod.abstracts.CustomRelic;
import com.badlogic.gdx.graphics.Texture;
import com.evacipated.cardcrawl.mod.stslib.relics.ClickableRelic;
import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
import com.megacrit.cardcrawl.actions.unique.AddCardToDeckAction;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import theStacker.TheStackerMod;
import theStacker.cards.skill.SecretGrade;
import theStacker.characters.TheStacker;
import theStacker.stacking.Matrix;
import theStacker.stacking.Mino;
import theStacker.util.TextureLoader;

import static theStacker.TheStackerMod.makeRelicOutlinePath;
import static theStacker.TheStackerMod.makeRelicPath;

public class BoxOfSecrets extends CustomRelic implements ClickableRelic {
    public static final String ID = TheStackerMod.makeID(BoxOfSecrets.class.getSimpleName() + "Relic");
    private static final Texture IMG_HIDDEN = TextureLoader.getTexture(makeRelicPath("placeholder_relic.png"));
    private static final Texture IMG_REVEALED = TextureLoader.getTexture(makeRelicPath("placeholder_relic.png"));
    private static final Texture OUTLINE = TextureLoader.getTexture(makeRelicOutlinePath("placeholder_relic.png"));

    public BoxOfSecrets() {
        super(ID, IMG_HIDDEN, OUTLINE, RelicTier.UNCOMMON, LandingSound.SOLID);
    }

    boolean running = false;

    @Override
    public void setCounter(int counter) {
        super.setCounter(counter);
        this.setTexture(counter > 0 ? IMG_REVEALED : IMG_HIDDEN);
        this.updateDescription(AbstractDungeon.player.chosenClass);
    }

    @Override
    public void onEquip() {
        setCounter(-1);
    }

    @Override
    public void atBattleStartPreDraw() {
        running = true;
        if (AbstractDungeon.player instanceof TheStacker) {
            TheStacker stacker = (TheStacker) AbstractDungeon.player;
            Matrix matrix = stacker.getMatrix();
            matrix.pieceLock.listen(this, () -> {
                int[] pattern = new int[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
                for (int y = 0; y < pattern.length; y++) {
                    for (int x = 0; x < matrix.board.getCols(); x++) {
                        boolean filled = matrix.board.getMino(y, x).isFilled();
                        boolean shouldFill = x != pattern[y];
                        if (filled != shouldFill)
                            return running;
                    }
                }
                matrix.reset(true);
                setCounter(1);
                addToBot(new RelicAboveCreatureAction(AbstractDungeon.player, this));
                addToBot(new AddCardToDeckAction(new SecretGrade()));
                return running;
            });
        }
    }

    @Override
    public void onRightClick() {
        // debugging
        if (AbstractDungeon.player instanceof TheStacker) {
            Matrix matrix = ((TheStacker) AbstractDungeon.player).getMatrix();
            int[] pattern = new int[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
            for (int y = 0; y < pattern.length; y++) {
                for (int x = 0; x < matrix.board.getCols(); x++) {
                    boolean shouldFill = x != pattern[y];
                    matrix.board.getMino(y,x).set(matrix.board, shouldFill ? Mino.GARBAGE : Mino.EMPTY);
                }
            }
        }
    }

    @Override
    public void onVictory() {
        running = false;
    }

    @Override
    public String getUpdatedDescription() {
        return counter > 0 ? DESCRIPTIONS[1] : DESCRIPTIONS[0];
    }
}
package theStacker.util;

import java.util.HashMap;
import java.util.function.Function;
import java.util.function.Supplier;

public class MicroEvent<T> {
    private HashMap<Object, Function<T, Boolean>> handlers = new HashMap<>();

    /**
     * Adds a listener for the event.
     * @param id The reference-equality ID of the listener. Duplicate registrations not allowed.
     * @param handler The handler callback. Returning false removes the listener.
     */
    public void listen(Object id, Supplier<Boolean> handler) {
        listen(id, (_ignored) -> handler.get());
    }

    /**
     * Adds a listener for the event.
     * @param id The reference-equality ID of the listener. Duplicate registrations not allowed.
     * @param handler The handler callback. Returning false removes the listener.
     */
    public void listen(Object id, Function<T, Boolean> handler) {
        handlers.put(id, handler);
    }

    /**
     * Fires the event, calling all associated handlers.
     */
    public void fire(T value) {
        handlers.entrySet().removeIf(objectSupplierEntry -> !objectSupplierEntry.getValue().apply(value));
    }
}

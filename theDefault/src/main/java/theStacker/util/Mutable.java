package theStacker.util;

import java.util.function.Function;

/**
 * Simple wrapper class for a value, mostly so I can modify it from inside a lambda
 * @param <T> The object type to wrap
 */
public class Mutable<T> {
    private T value;

    public Mutable(T value) {
        this.value = value;
    }

    public void set(T value) {
        this.value = value;
    }

    public T get() {
        return this.value;
    }

    public T update(Function<T, T> mutator) {
        this.value = mutator.apply(value);
        return this.value;
    }
}

package theStacker.util;

import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import theStacker.cards.status.PieceCard;
import theStacker.stacking.Board;

public class Helpers {
    private Helpers() {}

    public static void queuePieceCard(PieceCard card) {
        AbstractDungeon.player.drawPile.addToTop(card);
    }

    public static int powerStacks(AbstractCreature creature, String powerId) {
        if (creature == null) return 0;
        if (!creature.hasPower(powerId)) return 0;
        return creature.getPower(powerId).amount;
    }

    public static int minosFilled(Board board) {
        int filled = 0;
        for (int y = 0; y < board.getRows(); y++)
            for (int x = 0; x < board.getCols(); x++)
                if (board.getMino(y, x).isFilled())
                    filled++;
        return filled;
    }
}

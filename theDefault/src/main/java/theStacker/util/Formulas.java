package theStacker.util;

public class Formulas {
    public static int pieceExhaustDamage(int pieceCardsExhausted) {
        return pieceCardsExhausted + 1;
    }

    public static int lineClearDamage(int lines, int combo, int strength) {
        if (lines == 0) return 0; // Edge case: 5*(-1)^2 = 5
        return (int) (5*Math.pow(lines-1, 1.5) + comboDamage(combo, strength) + Math.min(strength, 0));
    }

    public static int comboDamage(int combo, int strength) {
        return (int) Math.floor(Math.max(strength+1, 1) * combo/5f);
    }
}

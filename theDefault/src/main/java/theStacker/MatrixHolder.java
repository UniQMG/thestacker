package theStacker;

import theStacker.stacking.Matrix;

/**
 * Represents something that has a playing matrix.
 */
public interface MatrixHolder {
    Matrix getMatrix();
}

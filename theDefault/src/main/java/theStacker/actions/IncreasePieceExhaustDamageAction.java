package theStacker.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import theStacker.characters.TheStacker;

import static theStacker.TheStackerMod.logger;

public class IncreasePieceExhaustDamageAction extends AbstractGameAction {
    @Override
    public void update() {
        if (AbstractDungeon.player instanceof TheStacker) {
            ((TheStacker) AbstractDungeon.player).pieceCardsExhausted++;
            logger.info("Piece cards exhausted: " + ((TheStacker) AbstractDungeon.player).pieceCardsExhausted);
        }
        isDone = true;
    }
}

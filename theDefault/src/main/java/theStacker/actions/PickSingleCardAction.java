package theStacker.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.utility.WaitAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import java.util.function.Consumer;

import static com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;

public class PickSingleCardAction extends AbstractGameAction {
//    private static final String prompt = CardCrawlGame.languagePack.getCardStrings(StackedQueue.ID).EXTENDED_DESCRIPTION[0];
    private String prompt;
    private Consumer<AbstractCard> acceptCard;
    private boolean opened = false;

    public PickSingleCardAction(String prompt, Consumer<AbstractCard> acceptCard) {
        this.prompt = prompt;
        this.acceptCard = acceptCard;
        this.duration = ACTION_DUR_FAST;
        this.actionType = ActionType.CARD_MANIPULATION;
    }

    @Override
    public void update() {
        AbstractPlayer player = AbstractDungeon.player;
        if (player.hand.isEmpty()) {
            isDone = true;
            return;
        }

        if (player.hand.size() == 1) {
            AbstractCard card = player.hand.group.get(0);
//            player.hand.removeCard(card);
            this.acceptCard.accept(card);
            isDone = true;
            return;
        }

        if (!opened) {
            // Apparently this like, pauses the action queue or something while open?
            AbstractDungeon.handCardSelectScreen.open(
                prompt,
                1,
                false,
                false,
                false,
                false,
                false
            );
            AbstractDungeon.actionManager.addToBottom(new WaitAction(0.25f));
            opened = true;
            return;
        }

        if (!AbstractDungeon.handCardSelectScreen.wereCardsRetrieved) {
            AbstractCard card = AbstractDungeon.handCardSelectScreen.selectedCards.group.get(0);
            this.acceptCard.accept(card);
            AbstractDungeon.handCardSelectScreen.wereCardsRetrieved = true;
        }

        tickDuration();
    }
}

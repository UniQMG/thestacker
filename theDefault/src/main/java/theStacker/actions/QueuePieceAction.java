package theStacker.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import theStacker.characters.TheStacker;
import theStacker.stacking.Matrix;
import theStacker.stacking.Piece;

public class QueuePieceAction extends AbstractGameAction {
    private Piece.PieceType type;

    public QueuePieceAction(Piece.PieceType type) {
        this.type = type;
    }

    @Override
    public void update() {
        if (AbstractDungeon.player instanceof TheStacker) {
            TheStacker stacker = (TheStacker) AbstractDungeon.player;
            stacker.getMatrix().queuePiece(Piece.get(type));
        }

        isDone = true;
    }
}

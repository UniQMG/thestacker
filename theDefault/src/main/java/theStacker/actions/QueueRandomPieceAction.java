package theStacker.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import theStacker.MatrixHolder;
import theStacker.stacking.Matrix;
import theStacker.stacking.Piece;

public class QueueRandomPieceAction extends AbstractGameAction {
    private final int amount;

    public QueueRandomPieceAction(int amount) {
        super();
        this.amount = amount;
    }

    @Override
    public void update() {
        if (!(AbstractDungeon.player instanceof MatrixHolder)) return;
        Matrix matrix = ((MatrixHolder) AbstractDungeon.player).getMatrix();
        for (int i = 0; i < amount; i++)
            matrix.queue.add(Piece.get(matrix.generator.next()));
        this.isDone = true;
    }
}
